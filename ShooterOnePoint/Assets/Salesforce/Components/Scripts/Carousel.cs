﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;

namespace Salesforce.Components
{
	public class Carousel : MonoBehaviour 
	{
		public enum Direction { Vertical, Horizontal };
		[SerializeField] RecordsListView listView;
		[SerializeField] Transform center;
		[SerializeField] float spacing;
		[SerializeField] float scrollSpeed;
		List<RecordView> recordViews = new List<RecordView>();
		float scroll;
		int tweenIndex;
		
		public void UpdatePositions()
		{
			recordViews.Clear();
			for (var i = 0; i < center.childCount; ++i)
			{
				var recordView = center.GetChild(i).GetComponent<RecordView>();
				if (recordView != null)
				{
					recordViews.Add(recordView);
				}
			}
			if (recordViews.Count == 0)
			{
				return;
			}

			var r = recordViews.Count * spacing / (Mathf.PI * 2);
			center.localPosition = new Vector3(0, 0, r); 
			
			var angle = 0f;
			var step = 2 * Mathf.PI / recordViews.Count;
			foreach (var recordView in recordViews)
			{
				recordView.transform.localPosition = new Vector3(Mathf.Sin(angle), 0, -Mathf.Cos(angle)) * r;
				angle += step;
			}
		}

		private float NextAngle(float angle, int direction)
		{
			if (recordViews.Count == 0)
			{
				return 0;
			}
			var step = 360f / recordViews.Count;
			var steps = Mathf.Round(angle / step) + direction;
			return step * steps;
		}

		public void ScrollRight()
		{
			ScrollTo(NextAngle(center.localEulerAngles.y, 1), Direction.Horizontal);
		}

		public void ScrollLeft()
		{
			ScrollTo(NextAngle(center.localEulerAngles.y, -1), Direction.Horizontal);
		}

//		public void ScrollUp()
//		{
//			ScrollTo(NextAngle(center.localEulerAngles.x, 1), Direction.Vertical);
//		}
//
//		public void ScrollDown()
//		{
//			ScrollTo(NextAngle(center.localEulerAngles.x, -1), Direction.Vertical);
//		}

		private void ScrollTo(float angle, Direction dir)
		{
			Vector3 rotation = dir == Direction.Horizontal ? new Vector3 (0, angle, 0) : new Vector3 (angle, 0, 0);
			if (scrollSpeed < float.Epsilon)
			{
				center.localEulerAngles = rotation;
			}
			else
			{
				listView.Unfocus();
				tweenIndex++;
				/*iTween.RotateTo(center.gameObject, iTween.Hash(
					"rotation", rotation,
					"easetype", iTween.EaseType.easeInOutSine,
					"speed", scrollSpeed,
					"islocal", true,
					"oncomplete", "CarouselFocus",
					"oncompletetarget", gameObject,
					"oncompleteparams", tweenIndex));*/
			}
		}

		public void CarouselFocus(int index)
		{
			if (index != tweenIndex)
			{
				return;
			}
			foreach (var v in recordViews)
			{
				if (Vector3.Distance(v.transform.position, transform.position) < spacing / 2f)
				{
					listView.Focus(v);
					break;
				}
			}
		}
	}
}
