﻿using UnityEngine;

namespace Salesforce.Components
{
	public class ClientExtension : MonoBehaviour 
	{
		[SerializeField] public Client client;
		
		protected virtual void Awake()
		{
			if (client == null)
			{
				client = FindObjectOfType<Client>();
			}
		}
	}
}
