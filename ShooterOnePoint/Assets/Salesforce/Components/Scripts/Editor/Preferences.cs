﻿using UnityEngine;
using UnityEngine.UI;
using UnityEditor.SceneManagement;
using UnityEditor;
using System.Collections.Generic;

namespace Salesforce.Editor
{
    [InitializeOnLoad]
    public static class Preferences
    {
        public static bool LearningMode
        {
            get
            {
                return EditorPrefs.GetInt("SalesforceLearningMode", 0) != 0;
            }
            set
            {
                EditorPrefs.SetInt("SalesforceLearningMode", value ? 1 : 0);
            }
        }

        public static string Username
        {
            get
            {
                return EditorPrefs.GetString("SalesforceUsername", "");
            }
            set
            {
                EditorPrefs.SetString("SalesforceUsername", value);
            }
        }

        public static string Password
        {
            get
            {
                return EditorPrefs.GetString("SalesforcePassword", "");
            }
            set
            {
                EditorPrefs.SetString("SalesforcePassword", value);
            }
        }

        public static string ClientID
        {
            get
            {
                return EditorPrefs.GetString("SalesforceClientID", "");
            }
            set
            {
                EditorPrefs.SetString("SalesforceClientID", value);
            }
        }

        public static string ClientSecret
        {
            get
            {
                return EditorPrefs.GetString("SalesforceClientSectet", "");
            }
            set
            {
                EditorPrefs.SetString("SalesforceClientSectet", value);
            }
        }
        

        [PreferenceItem("Salesforce VR")]
        static void SalesForcePreferenceItem()
        {
            EditorGUILayout.BeginVertical();

            EditorGUILayout.HelpBox("Here you can enter your Client ID, Client Secret, Username and Password to speed up app example initialization and new scene creation. Every time you create a Salesforce component that requires any of those values, it will be automatically used.", MessageType.Info);
            EditorGUILayout.HelpBox("Note that if you don't want to have default username and/or password pre-entered in your login forms, keep those fields blank. None of those fields are mandatory in this settings.", MessageType.Info);
            if (GUILayout.Button("Go to Salesforce Developer Dashboard"))
            {
                Application.OpenURL("http://developer.salesforce.com");
            }

            EditorGUILayout.Space();
            GUILayout.Label("Client ID");
            var style = new GUIStyle(GUI.skin.textArea);
            style.fixedHeight = 0;
            style.stretchWidth = true;
            style.stretchHeight = false;
            style.wordWrap = true;
            ClientID = EditorGUILayout.TextArea(ClientID, style);
            EditorGUILayout.Space();
            ClientSecret = EditorGUILayout.TextField(new GUIContent("Client Secret"), ClientSecret);
            EditorGUILayout.Space();
            Username = EditorGUILayout.TextField(new GUIContent("Username"), Username);
            EditorGUILayout.Space();
            Password = EditorGUILayout.PasswordField(new GUIContent("Password"), Password);
            EditorGUILayout.Space();
            LearningMode = EditorGUILayout.Toggle("Learning Mode", LearningMode);
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.Space();

            if (GUILayout.Button("Apply to Examples"))
            {
                ApplyToExample("Assets/Salesforce/Core/Examples/LoginExample.unity");
                ApplyToExample("Assets/Salesforce/Core/Examples/QueryExample.unity");

                ApplyToExample("Assets/Salesforce/Playmaker/Examples/LoginExample.unity");
                ApplyToExample("Assets/Salesforce/Playmaker/Examples/QueryExample.unity");
                ApplyToExample("Assets/Salesforce/Playmaker/Examples/AccountContactsChart/AccountContactsChart.unity");

                ApplyToExample("Assets/Salesforce/Components/Examples/AccountContactsExample.unity");
                ApplyToExample("Assets/Salesforce/Components/Examples/LoginFormExample.unity");
                ApplyToExample("Assets/Salesforce/Components/Examples/RecordsEditorExample.unity");
                ApplyToExample("Assets/Salesforce/Components/Examples/RecordsCarouselExample.unity");
                ApplyToExample("Assets/Salesforce/Components/Examples/RecordsListExample.unity");
                ApplyToExample("Assets/Salesforce/Components/Examples/SilentLoginExample.unity");
            }

            if (GUILayout.Button("Apply to Open Scene"))
            {
                ApplyToOpenScene();
            }

            EditorGUILayout.EndVertical();
        }

        static void ApplyToOpenScene()
        {
            foreach (var client in GameObject.FindObjectsOfType<Client>())
            {
                if (!string.IsNullOrEmpty(ClientID))
                {
                    client.clientId = ClientID;
                }
                if (!string.IsNullOrEmpty(ClientSecret))
                {
                    client.clientSecret = ClientSecret;
                }
            }

            bool loginProcessed = false;

            foreach (var silentLogin in GameObject.FindObjectsOfType<Components.SilentLogin>())
            {
                loginProcessed = true;
                if (!string.IsNullOrEmpty(Username))
                {
                    silentLogin.username = Username;
                }
                if (!string.IsNullOrEmpty(Password))
                {
                    silentLogin.password = Password;
                }
            }

            foreach (var loginForm in GameObject.FindObjectsOfType<Components.UguiLoginForm>())
            {
                loginProcessed = true;
                if (!string.IsNullOrEmpty(Username))
                {
                    loginForm.username.text = Username;
                }
                if (!string.IsNullOrEmpty(Password))
                {
                    loginForm.password.text = Password;
                }
            }

            if (!loginProcessed)
            {
                var inputs = new Dictionary<string, InputField>(); 
                foreach (var input in GameObject.FindObjectsOfType<InputField>())
                {
                    inputs.Add(input.name, input);
                }
                if (inputs.ContainsKey("Username") && !string.IsNullOrEmpty(Username))
                {
                    inputs["Username"].text = Username;
                }
                if (inputs.ContainsKey("Password") && !string.IsNullOrEmpty(Password))
                {
                    inputs["Password"].text = Password;
                }
            }

            EditorSceneManager.MarkAllScenesDirty();
        }

        static void ApplyToExample(string sceneName)
        {
            EditorSceneManager.OpenScene(sceneName);
            ApplyToOpenScene();
            EditorSceneManager.SaveOpenScenes();
        }
    }
}
