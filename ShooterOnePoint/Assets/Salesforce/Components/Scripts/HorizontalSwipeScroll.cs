﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Events;

namespace Salesforce.Components
{
	public class HorizontalSwipeScroll : MonoBehaviour
	{
		public UnityEvent OnSwipeLeft;
		public UnityEvent OnSwipeRight;

		public void Swipe(SwipeDetection.SwipeDirection dir)
		{
			switch (dir) 
			{
			case SwipeDetection.SwipeDirection.Left:
				OnSwipeLeft.Invoke ();
				break;
			case SwipeDetection.SwipeDirection.Right:
				OnSwipeRight.Invoke ();
				break;
			default:
				break;
			}
		}
	}
}
