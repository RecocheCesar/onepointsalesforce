﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Salesforce.Components
{
	public class HorizontalTapScroll : MonoBehaviour 
	{
		public Button scrollLeft;
		public Button scrollRight;
	}
}
