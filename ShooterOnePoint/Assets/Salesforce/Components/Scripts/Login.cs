﻿using UnityEngine;
using UnityEngine.Events;

namespace Salesforce.Components
{
	public class Login : ClientExtension 
	{
		[SerializeField] public UnityEvent onLogin;

		public bool IsLoggedIn
		{
			get
			{
				if (client == null)
				{
					return false;
				}
				return client.LoggedIn;
			}
		}
	}
}
