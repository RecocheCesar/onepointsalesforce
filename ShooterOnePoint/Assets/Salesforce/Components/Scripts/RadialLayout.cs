﻿using UnityEngine;

namespace Salesforce.Components
{
	public enum Axis
	{
		XY = 0,
		XZ = 10,
		YX = 20,
		YZ = 30,
		ZX = 40,
		ZY = 50,
	}

	public class RadialLayout : MonoBehaviour 
	{
		[SerializeField] RecordsListView listView;
		[SerializeField] Axis axis;
		[SerializeField] float radius;
		[SerializeField] float spacing;
		
		public Vector3 GetDirection(float w, float h)
		{
			switch (axis)
			{
				case Axis.XY: return new Vector3(w, h, 0);
				case Axis.XZ: return new Vector3(w, 0, h);
				case Axis.YX: return new Vector3(h, w, 0);
				case Axis.YZ: return new Vector3(0, w, h);
				case Axis.ZX: return new Vector3(h, 0, w);
				case Axis.ZY: return new Vector3(0, h, w);
			}
			return Vector3.zero;
		}

		void Start()
		{
			UpdatePositions();
		}
		
		public void UpdatePositions()
		{
			if (transform.childCount == 0)
			{
				return;
			}

			var r = transform.childCount * spacing / (Mathf.PI * 2);
			if (spacing < float.Epsilon)
			{
				r = radius;
			}
			else
			{
				r = Mathf.Max(r, radius);
			}
			
			var angle = 0f;
			var step = 2 * Mathf.PI / transform.childCount;
			for (var i = 0; i < transform.childCount; ++i)
			{
				var w = Mathf.Sin(angle);
				var h = -Mathf.Cos(angle);
				
				transform.GetChild(i).localPosition = GetDirection(w, h) * r;
				angle += step;
			}
		}
	}
}
