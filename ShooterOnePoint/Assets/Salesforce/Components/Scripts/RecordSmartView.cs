﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Globalization;

namespace Salesforce.Components
{
	public class RecordSmartView : RecordView 
	{
	  private static Color HexToColor(string hex)
	  {
	    hex = hex.Replace("0x", ""); //in case the string is formatted 0xFFFFFF
	    hex = hex.Replace("#", ""); //in case the string is formatted #FFFFFF
	    var symbolsQuantity = 2; //specifying rgba(rgb) or rrggbbaa(rrggbb)
	    if (hex.Length == 3 || hex.Length == 4)
	    {
	      symbolsQuantity = 1;
	    }
	    byte a = 255; //assume fully visible unless specified in hex
	    byte r = byte.Parse(hex.Substring(0, symbolsQuantity), NumberStyles.HexNumber);
	    byte g = byte.Parse(hex.Substring(symbolsQuantity, symbolsQuantity), NumberStyles.HexNumber);
	    byte b = byte.Parse(hex.Substring(symbolsQuantity * 2, symbolsQuantity), NumberStyles.HexNumber);
	    //Only use alpha if the string has enough characters
	    if (hex.Length == 4 || hex.Length == 8)
	    {
	      a = byte.Parse(hex.Substring(symbolsQuantity * 4, symbolsQuantity), NumberStyles.HexNumber);
	    }
	    return new Color32(r, g, b, a);
	  }

	  private static Color ParseColor(string color)
	  {
	    int output;
	    if (int.TryParse(color, NumberStyles.HexNumber, null, out output))
	    {
	      return HexToColor(color);
	    }
		switch(color)
			{
				case "black": return Color.black;
				case "blue": return Color.blue;
				case "cyan": return Color.cyan;
				case "gray": return Color.gray;
				case "grey": return Color.grey;
				case "green": return Color.green;
				case "magenta": return Color.magenta;
				case "red": return Color.red;
				case "white": return Color.white;
				case "yellow": return Color.yellow;
			}
			
			return Color.white;
		}

		public override void SetModel(Record record)
		{
			foreach (var t in GetComponentsInChildren<Transform>())
			{
				if (t.name.Contains("$"))
				{
					var key = t.name.Substring(t.name.LastIndexOf("$") + 1);
					var value = record.GetFieldValue(key);
					if (value != null)
					{
						ApplyTextValue(t.gameObject, value);
					}
					var subRecords = record.GetSubRecords(key);
					if (subRecords != null)
					{
						ApplySubRecords(t.gameObject, subRecords);
					}
				}
				if (t.name.Contains("#"))
				{
					var colorsMap = new Dictionary<string, Color>();
					var index = t.name.LastIndexOf("#");
					var mapLines = t.name.Substring(0, index).Split(',');
					foreach (var mapLine in mapLines)
					{
						var line = mapLine.Split('=');
						var colorKey = line[0].Trim();
						var colorValue = ParseColor(line[1].Trim());
						colorsMap[colorKey] = colorValue;
					}
					
					var key = t.name.Substring(index + 1);
					var value = record.GetFieldValue(key);

					if (colorsMap.ContainsKey(value))
					{
						ApplyColorValue(t.gameObject, colorsMap[value]);
					}
				}
			}
			foreach (var button in GetComponentsInChildren<Button>())
			{
				if (button.name.ToLower() == ">select")
				{
					button.onClick.AddListener(Select);
				}
			}
			/*foreach (var trigger in GetComponentsInChildren<Trigger>())
			{
				var action = trigger.name;
				trigger.OnTriggered.AddListener(() => OnActionTrigger.Invoke(action));
			}*/
		}

		public static void ApplyTextValue(GameObject go, string value)
		{
			var text = go.GetComponent<Text>();
			if (text != null)
			{
				text.text = value;
			}

			var textMesh = go.GetComponent<TextMesh>();
			if (textMesh != null)
			{
				textMesh.text = value;
			}
		}

		public static void ApplyColorValue(GameObject go, Color value)
		{
			var text = go.GetComponent<Text>();
			if (text != null)
			{
				text.color = value;
			}

			var textMesh = go.GetComponent<TextMesh>();
			if (textMesh != null)
			{
				textMesh.color = value;
			}

			foreach (var renderer in go.GetComponents<Renderer>())
			{
				renderer.material.color = value;
			}
		}

		public static void ApplySubRecords(GameObject go, RecordList records)
		{
			var list = go.GetComponent<RecordsListView>();
			if (list != null)
			{
				list.SetRecords(records);
			}
		}
	}
}
