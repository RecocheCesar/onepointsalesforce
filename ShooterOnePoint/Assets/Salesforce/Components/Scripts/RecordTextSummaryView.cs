﻿using UnityEngine;
using UnityEngine.UI;

namespace Salesforce.Components
{
	public class RecordTextSummaryView : RecordView 
	{
		public override void SetModel(Record record)
		{
			var summary = "";
			foreach (var field in record)
			{
				if (summary.Length > 0)
				{
					summary += "\n";
				}
				summary += field.Name + ": " + field.Value;
			}

			RecordSmartView.ApplyTextValue(gameObject, summary);
		}
	}
}
