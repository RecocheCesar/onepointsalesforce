﻿using UnityEngine;
using UnityEngine.Events;

namespace Salesforce.Components
{
	public abstract class RecordView : MonoBehaviour 
	{
		// Invoked from within the control when its action was triggered.
		// Who is subscribed? Record views container
		public ActionEvent OnActionTrigger;

		// Invoked from within the control when it 
		// indicates that it wants to become selection.
		// Since it may also require deselection of sibling rerord views,
		// it has to be handled outside, in the records container view
		// Who invokes? Rerord view itself
		// Who is subscribed? Record views container
		public UnityEvent OnSelect;

		// Those are invoked by the records container view, when selection changes
		// is is not necessary happening when the record is interacted with
		// for example deselection happens automatically when other record is selected
		// Or for some types of containers, it could be an auto-selection scenario
		// Who invokes? Record views container
		// Who is subscribed? Record itself to reflect "selectedness" change visually (if needed)
		public UnityEvent OnSelected;
		public UnityEvent OnDeselected;

		// This one is always called when record view is created
		public abstract void SetModel(Record record);

		// Should only be called by control itself
		public void Select()
		{
			OnSelect.Invoke();
		}
		
		// Should only be called by items container
		public void SetSelected()
		{
			OnSelected.Invoke();
		}

		// Should only be called by items container
		public void SetDeselected()
		{
			OnDeselected.Invoke();
		}

		// Should only be called by items container
		public void SetFocused()
		{
			
		}

		// Should only be called by items container
		public void SetUnfocused()
		{
			
		}
	}
}
