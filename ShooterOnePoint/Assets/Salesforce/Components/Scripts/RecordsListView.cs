﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;

namespace Salesforce.Components
{
	[System.Serializable]
	public class RecordEvent : UnityEvent<Record> { }

	[System.Serializable]
	public class ActionEvent : UnityEvent<string> { }

	[System.Serializable]
	public class RecordActionEvent : UnityEvent<Record, string> { }

	[System.Serializable]
	public class ContentChangeEvent : UnityEvent<Dictionary<Record, RecordView>> { }

	public class RecordsListView : ClientExtension 
	{
		[TextArea(3,10)]
		public string query;
		public GameObject itemPrefab;
		[SerializeField] Transform container;
		public bool focusOnInit;
		public bool selectOnFocus;
		public bool deselectOnUnfocus;
		public RecordEvent onRecordSelect;
		public RecordActionEvent onRecordAction;
		public UnityEvent onContentChange;
		Dictionary<Record, RecordView> recordViews = new Dictionary<Record, RecordView>();
		Dictionary<RecordView, Record> records = new Dictionary<RecordView, Record>();
		Record selected;
		Record focused;

		protected override void Awake()
		{
			base.Awake();
			client.OnQueryFailed.AddListener(OnQueryFailed);
			client.OnQuerySucceeded.AddListener(OnQuerySucceeded);
		}

		public void Refresh()
		{
			ClearContainer();
			client.Query(query);
		}

		public void SetRecords(RecordList records)
		{
			ClearContainer();
			AddRecords(records);
		}

		void ClearContainer()
		{
			Select((Record)null);
			Focus((Record)null);
			recordViews.Clear();
			records.Clear();
			while (container.childCount > 0)
			{
				DestroyImmediate(container.GetChild(0).gameObject);
			}
			onContentChange.Invoke();
		}

		void AddItemToContainer(GameObject item)
		{
			item.transform.SetParent(container);
			item.transform.localScale = Vector3.one;
		}

		GameObject InstantiateItem(Record record)
		{
			var go = Instantiate(itemPrefab);
			var recordView = go.GetComponent<RecordView>();
			if (recordView != null)
			{
				recordView.SetModel(record);
				recordView.OnSelect.AddListener(() => Select(record));
				recordView.OnActionTrigger.AddListener((action) => onRecordAction.Invoke(record, action));
				recordViews.Add(record, recordView);
				records.Add(recordView, record);
			}
			return go;
		}

		public void Deselect()
		{
			Select((Record)null);
		}

		public void Unfocus()
		{
			Focus((Record)null);
		}

		public void Focus(RecordView recordView)
		{
			Focus(recordView != null ? records[recordView] : null);
		}

		public void Focus(Record record)
		{
			if (focused == record)
			{
				return;
			}

			if (focused != null)
			{
				recordViews[focused].SetUnfocused();
				if (deselectOnUnfocus)
				{
					Select((Record)null);
				}
			}

			focused = record;

			if (focused != null)
			{
				recordViews[focused].SetFocused();
				if (selectOnFocus)
				{
					Select(record);
				}
			}
		}

		public void Select(RecordView recordView)
		{
			Select(recordView != null ? records[recordView] : null);
		}

		public void Select(Record record)
		{
			if (selected == record)
			{
				return;
			}

			if (selected != null)
			{
				recordViews[selected].SetDeselected();
			}

			selected = record;

			if (selected != null)
			{
				recordViews[selected].SetSelected();
			}

			onRecordSelect.Invoke(record);
		}

		void OnQueryFailed(Response response)
		{
			Debug.LogWarning("Query failed: " + response.message);
		}

		void OnQuerySucceeded(Response response, RecordList records)
		{
			AddRecords(records);
		}

		void AddRecords(RecordList records)
		{
			foreach (var record in records)
			{
				AddItemToContainer(InstantiateItem(record));
			}
			onContentChange.Invoke();

			if (focusOnInit && records.Count > 0)
			{
				Focus(records[0]);
			}
		}
	}
}
