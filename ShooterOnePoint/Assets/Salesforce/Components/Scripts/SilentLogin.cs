﻿using UnityEngine;

namespace Salesforce.Components
{
	public class SilentLogin : Login 
	{
		[SerializeField] bool loginOnStart = true;
		public string username;
		public string password;

		protected override void Awake()
		{
			base.Awake();
			client.OnLoginFailed.AddListener(OnLoginFailed);
			client.OnLoginSucceeded.AddListener(OnLoginSucceeded);
		}

		void Start() 
		{
			if (loginOnStart)
			{
				Login();
			}
		}

		public void Login()
		{
			client.Login(username, password);
		}

		void OnLoginFailed(Response response)
		{
			Debug.LogWarning("Failed to login to salesforce:\n" + response.message);
		}

		void OnLoginSucceeded(Response response)
		{
			onLogin.Invoke();
		}
	}
}
