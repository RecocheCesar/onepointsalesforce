﻿using UnityEngine;
using System.Collections;

namespace Salesforce.Components
{
    public class SwipeDetection : MonoBehaviour
    {
        [System.Serializable]
        public class SwipeEvent : UnityEngine.Events.UnityEvent<SwipeDirection> { }

        public enum SwipeDirection { Up, Down, Left, Right };
        public SwipeEvent OnSwipe;

        private SwipeDirection swipeDirection;

        private Vector2 swipeStart;
        private Vector2 swipeEnd;
        private Vector2 currentSwipe;

        //private float swipeMinTime;
        private float swipeMinDist = 50f;

        void Update()
        {
            DetectSwipe();
        }

        private void DetectSwipe()
        {
            //#if UNITY_IPHONE || UNITY_ANDROID

            if (Input.touchCount > 0)
            {
                var t = Input.GetTouch(0);
                if (t.phase == TouchPhase.Began)
                {
                    swipeStart = t.position;
                }
                if (t.phase == TouchPhase.Ended)
                {
                    swipeEnd = t.position;
                    currentSwipe = swipeEnd - swipeStart;

                    if (currentSwipe.magnitude < swipeMinDist)
                    {
						return;
                    }

                    if (Mathf.Abs(currentSwipe.x) > Mathf.Abs(currentSwipe.y))
                    {
                        if (currentSwipe.x > 0)
                            OnSwipe.Invoke(SwipeDirection.Right);
                        else
                            OnSwipe.Invoke(SwipeDirection.Left);
                    }
                    else
                    {
                        if (currentSwipe.y > 0)
                            OnSwipe.Invoke(SwipeDirection.Up);
                        else
                            OnSwipe.Invoke(SwipeDirection.Down);
                    }
                }
            }
            //#endif
        }
    }
}
