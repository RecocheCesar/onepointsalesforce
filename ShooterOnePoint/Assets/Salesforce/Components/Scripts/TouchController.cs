﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Salesforce.Components
{
	public class TouchController : MonoBehaviour 
	{
        [SerializeField]
        private float rayLength = 30f;

        public UnityEvent onTouchLeft;
        public UnityEvent onTouchRight;

		void Update()
		{
			CastRay();
		}

		void CastRay()
        {
            Vector3 touchPosition;
#if UNITY_EDITOR
            if (!Input.GetMouseButtonUp(0))
            {
                return;
            }
            touchPosition = Input.mousePosition;
#else
			if (Input.touches.Length <= 0)
		    {
                return;
            }
            
			var myTouch = Input.GetTouch(0);
			if (myTouch.phase != TouchPhase.Ended)
			{
                return;
            }

            touchPosition = new Vector3(myTouch.position.x, myTouch.position.y);
#endif
            
            RaycastHit hitInfo;
            EventTrigger trigger = null;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(touchPosition), out hitInfo, rayLength))
            {
                trigger = hitInfo.collider.GetComponent<EventTrigger>();
                trigger.OnPointerClick(null);
            }

            if (trigger != null)
            {
                return;
            }

            if (touchPosition.x <= Screen.width / 2)
            {
                onTouchLeft.Invoke();
            }
            else
            {
                onTouchRight.Invoke();
            }   
        }
	}
}
