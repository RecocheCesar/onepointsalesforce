﻿using UnityEngine;
using UnityEngine.UI;

namespace Salesforce.Components
{
	public class UguiLoginForm : Login 
	{
		public InputField username;
		public InputField password;
		[SerializeField] Button loginButton;
		[SerializeField] Text message;
		[SerializeField] bool hideAfterLogin = true;
		
		protected override void Awake()
		{
			base.Awake();
			client.OnLoginFailed.AddListener(OnLoginFailed);
			client.OnLoginSucceeded.AddListener(OnLoginSucceeded);
		}

		public void Login()
		{
			if (message != null)
			{
				message.text = "Logging In ...";
			}
			username.interactable = false;
			password.interactable = false;
			loginButton.interactable = false;
			client.Login(username.text, password.text);
		}

		void OnLoginFailed(Response response)
		{
			if (message != null)
			{
				message.text = "Failed to login. Check console for details.";
			}
			username.interactable = true;
			password.interactable = true;
			loginButton.interactable = true;
		}

		void OnLoginSucceeded(Response response)
		{
			if (message != null)
			{
				message.text = "Logged In Successfully.";
			}
			username.interactable = true;
			password.interactable = true;
			loginButton.interactable = true;
			onLogin.Invoke();
			if (hideAfterLogin)
			{
				gameObject.SetActive(false);
			}
		}
	}
}
