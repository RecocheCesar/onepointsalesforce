﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections.Generic;

namespace Salesforce.Components
{
	public class UguiRecordEditor : ClientExtension
    {
        public UnityEvent OnEditingComplete;
        [SerializeField] Transform container;
        [SerializeField] GameObject recordFieldPrefab;
        [SerializeField] Button submitButton;
        [SerializeField] Text message;

        private List<GameObject> recordFields;
		private Record record;

        protected override void Awake()
		{
            base.Awake();
            recordFields = new List<GameObject>();
        }

		public void ShowRecordFields(Record rec)
		{
			record = rec;
            for (var f = 0; f < record.Count; f++)
            {
                if (recordFields.Count < record.Count)
                { 
                    GameObject recordField = Instantiate(recordFieldPrefab);
                    recordField.transform.SetParent(container);
                    recordFields.Add(recordField);
                }
				recordFields[f].transform.FindChild ("Title").GetComponent<Text> ().text = record[f].Name;
				var inputField = recordFields[f].GetComponentInChildren<InputField> ();
				inputField.text = record[f].Value;     
			}
		}

		public void Submit()
		{
            client.OnUpsertFailed.AddListener(OnUpsertFailed);
            client.OnUpsertSucceeded.AddListener(OnUpsertSucceded);
            client.UpsertRecord (record);
		}

        void OnUpsertFailed(Response response)
        {
            Debug.LogWarning(response.message);
        }

        void OnUpsertSucceded(Response response, Record rec)
        {
            OnEditingComplete.Invoke();
            Debug.Log("Upsert succesfull");
            client.OnUpsertFailed.RemoveAllListeners();
            client.OnUpsertSucceeded.RemoveAllListeners();
        }
	}
}
