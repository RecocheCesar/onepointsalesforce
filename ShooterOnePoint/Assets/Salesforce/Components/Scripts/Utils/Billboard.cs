﻿using UnityEngine;
using System.Collections;

namespace Salesforce.Components
{
	public class Billboard : MonoBehaviour 
	{
		new Camera camera;

		void Start()
		{
			if (camera == null)
			{
				camera = Camera.main;
			}
		}

		void LateUpdate()
		{
			if (camera != null)
			{
				transform.LookAt(2 * transform.position - camera.transform.position);
			}
		}
	}
}
