﻿using UnityEngine;
using System.Threading;

namespace Salesforce
{
	public static class ThreadDetector 
	{
		public static bool IsMainThread
		{
			get
			{
				return Thread.CurrentThread.ManagedThreadId <= 1; 
			} 
		}
	}
}
