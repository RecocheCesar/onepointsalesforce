﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Salesforce.Components
{
	public class VerticalTapScroll : MonoBehaviour 
	{
		public Button scrollUp;
		public Button scrollDown;
	}
}
