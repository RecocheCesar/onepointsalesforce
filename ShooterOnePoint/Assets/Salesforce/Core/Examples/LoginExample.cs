﻿using UnityEngine;
using UnityEngine.UI;

public class LoginExample : MonoBehaviour 
{
	[SerializeField] Salesforce.Client client;
	[SerializeField] InputField username;
	[SerializeField] InputField password;
	[SerializeField] Button loginButton;
	[SerializeField] Text message;
	[SerializeField] GameObject loginForm;
	
	public void Login() 
	{
		message.text = "Logging In ...";
		username.interactable = false;
		password.interactable = false;
		loginButton.interactable = false;
		client.Login(username.text, password.text);
	}
	
	public void OnLoginSucceeded(Salesforce.Response response)
	{
		message.text = "Logged In Successfully.";
		username.interactable = true;
		password.interactable = true;
		loginButton.interactable = true;
		loginForm.SetActive(false);
	}
	
	public void OnLoginFailed(Salesforce.Response response)
	{
		message.text = "Failed to login. Check console for details.";
		username.interactable = true;
		password.interactable = true;
		loginButton.interactable = true;
	}
}
