using UnityEngine;
using UnityEngine.UI;

public class QueryExample : MonoBehaviour 
{
	[SerializeField] Salesforce.Client client;
	
	[SerializeField] InputField username;
	[SerializeField] InputField password;
	[SerializeField] Button loginButton;
	[SerializeField] GameObject loginForm;
	
	[SerializeField] InputField query;
	[SerializeField] Button queryButton;
	[SerializeField] GameObject queryForm;
	
	[SerializeField] QueryExampleItem itemPrefab;
	[SerializeField] GameObject resultsPanel;
	[SerializeField] RectTransform itemsContainer;
	
	[SerializeField] Text message;
	
	public void Login() 
	{
		message.text = "Logging In ...";
		username.interactable = false;
		password.interactable = false;
		loginButton.interactable = false;
		client.Login(username.text, password.text);
	}
	
	public void OnLoginSucceeded(Salesforce.Response response)
	{
		message.text = "Logged In Successfully.";
		username.interactable = true;
		password.interactable = true;
		loginButton.interactable = true;
		loginForm.SetActive(false);
		queryForm.SetActive(true);
	}
	
	public void OnLoginFailed(Salesforce.Response response)
	{
		message.text = "Failed to login. Check console for details.";
		username.interactable = true;
		password.interactable = true;
		loginButton.interactable = true;
	}
	
	public void Query() 
	{
		message.text = "Querying data...";
		query.interactable = false;
		queryButton.interactable = false;
		
		while (itemsContainer.childCount > 0)
		{
			DestroyImmediate(itemsContainer.GetChild(0).gameObject);
		}
		client.Query(query.text);
	}
	
	public void OnQuerySucceeded(Salesforce.Response response, Salesforce.RecordList data)
	{
		message.text = "Query succeded.";
		query.interactable = true;
		queryButton.interactable = true;
		resultsPanel.SetActive(true);

		foreach (Salesforce.Record row in data) 
		{
			var item = Instantiate(itemPrefab);
			item.SetData(row);
			item.transform.SetParent(itemsContainer);
			item.transform.localScale = Vector3.one;
		}
	}
	
	public void OnQueryFailed(Salesforce.Response response)
	{
		message.text = "Failed to query data.";
		query.interactable = true;
		queryButton.interactable = true;
	}
}