﻿using UnityEngine;
using UnityEngine.UI;

public class QueryExampleItem : MonoBehaviour 
{
	[SerializeField] Text nameText;
	
	public void SetData(Salesforce.Record record)
	{
		nameText.text = record.GetFieldValue("Name");
	} 
}
