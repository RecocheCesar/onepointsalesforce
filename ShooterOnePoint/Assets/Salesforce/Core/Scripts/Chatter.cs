﻿namespace Salesforce 
{
	/*
	incomplete 
	*/
	public class FeedEvent : UnityEngine.Events.UnityEvent<Response, Record> { }

	public partial class Client
	{
		public FeedEvent OnFeedSucceeded;
		public FailureEvent OnFeedFailed;

		public void GetFeed() {
			string id = chatterId;
			var url = MakeUrl("/chatter/feeds/record/" + id + "/feed-elements");
			var headers = MakeHeaders("GET");

			StartCoroutine(RequestCoroutine(url, null, headers, (response) =>
			{
				OnFeedFailed.Invoke(response);
			},
			(www) =>
			{
				Log(www.text);
				OnFeedSucceeded.Invoke(new Response(www), null);
			}));
		}
	}
}

