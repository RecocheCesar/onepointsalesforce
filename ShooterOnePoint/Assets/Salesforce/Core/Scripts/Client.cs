using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Salesforce 
{
	public partial class Client : MonoBehaviour
	{
		[SerializeField] string apiVersion = "v36.0";
		[SerializeField] string endpoint = "https://login.salesforce.com/services/oauth2/token";
		public 			 string clientId = "";
		public			 string clientSecret = "";
		[SerializeField] bool 	verbose = true;
						 string token;
						 string baseUrl;
						 string userId;
						 string chatterId;
		
#if UNITY_EDITOR
		void Start()
		{
			CheckClientForErrors(this);
		}

		static bool IsClientSecret(string s)
		{
			if (string.IsNullOrEmpty(s))
			{
				return false;
			}
			var allDigits = true;
			foreach (var c in s)
			{
				if (!char.IsDigit(c))
				{
					allDigits = false;
				}
			}
			return (allDigits && s.Length > 12 && s.Length < 24);
		}

		static void CheckClientForErrors(Client client)
		{
			if (string.IsNullOrEmpty(client.clientId))
			{
				Debug.LogWarning("[Salesforce] Client ID is empty", client.gameObject);
			}
			else if (IsClientSecret(client.clientId))
			{
				Debug.LogWarning("[Salesforce] Seems like you entered Client Secret into the Client ID field");
			}
			else if (client.clientId.Length < 60 || client.clientId.Length > 100)
			{
				Debug.LogWarning("[Salesforce] Seems like Client ID is wrong");
			}

			if (string.IsNullOrEmpty(client.clientSecret))
			{
				Debug.LogWarning("[Salesforce] Client Secret is empty");
			}
			else if (!IsClientSecret(client.clientSecret))
			{
				Debug.LogWarning("[Salesforce] Seems like Client Secret is wrong");
			}	
		}
#endif

		string MakeUrl(string details)
		{
			return string.Format("{0}/services/data/{1}{2}", baseUrl, apiVersion, details);
		}
		
		Dictionary<string, string> MakeHeaders(string method)
		{
			return new Dictionary<string, string> ()
			{
				//{ "Content-Type", "application/json" },
                { "Content-Type", "application/x-www-form-urlencoded" },
                { "Authorization", "Bearer " + token },
				{ "X-HTTP-Method-Override", method },
			};
		}
		
		IEnumerator RequestCoroutine(string url, byte[] body, Dictionary<string, string> headers, System.Action<Response> onFailure, System.Action<WWW> onSuccess) 
		{
			if (!LoggedIn)
			{
				LogWarning("Request Failed: Not logged in");
				onFailure(NotLoggedIn);
				yield break;
			}
			
			WWW www = new WWW (url, body, headers);
			yield return www;

			var headersStr = "";
			foreach(var h in headers)
			{
				headersStr += "    " + h.Key + " = " + h.Value + "\n";
			}
			var bodyStr = "";
			if (body != null)
			{
				bodyStr = System.Text.Encoding.UTF8.GetString(body);
			}

			Log("Request: <color=green>" + url + "</color>\n\n" + 
				"Headers:\n" + headersStr + "\n" +
				"Request Body:\n" + bodyStr + "\n\n" +
				"Response Error: <color=red>" + www.error + "</color>\n" +
				"Response Body:\n" + www.text + "\n\n");

			if (!string.IsNullOrEmpty(www.error)) 
			{
				LogWarning("Request Failed: " + www.error);
				onFailure(new Response(www));
				yield break;
			}
			
			onSuccess(www);
		}
		
		IEnumerator JsonRequestCoroutine(string url, byte[] body, Dictionary<string, string> headers, System.Action<Response> onFailure, System.Action<WWW, JSONObject> onSuccess) 
		{
			yield return StartCoroutine(RequestCoroutine(url, body, headers, onFailure, (www) =>
			{
				var responseJson  = JSONObject.Parse(www.text);
				if (responseJson == null)
				{
					LogWarning("Request Failed: Bad response");
					onFailure(NotJson);
				}
				else
				{
					onSuccess(www, responseJson);
				}
			}));
		}
	}
}
