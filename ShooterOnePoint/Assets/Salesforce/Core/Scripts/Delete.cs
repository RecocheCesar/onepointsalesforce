﻿namespace Salesforce 
{
	[System.Serializable]
	public class DeleteEvent : UnityEngine.Events.UnityEvent<Response> { }
	
	public partial class Client
	{
		public DeleteEvent OnDeleteSucceeded;
		public FailureEvent OnDeleteFailed;

		public void DeleteRecord(Record record) {
			if (record.Type != null && record.GetFieldValue("id")!=null) {
				DeleteRecord (record.Type, record.GetFieldValue("id"));
			}
		}

		public void DeleteRecord(string obj, string record)
		{
			var url = MakeUrl ("/sobjects/" + obj + "/" + record + "?_HttpMethod=DELETE");
			var headers = MakeHeaders("POST");
			var body = System.Text.Encoding.UTF8.GetBytes("DELETE");

            StartCoroutine(RequestCoroutine(url, body, headers, (response) =>
			{
				OnDeleteFailed.Invoke(response);
			}
			, (www) =>
			{
				OnDeleteSucceeded.Invoke(new Response(www));
			}));
		}
	}
}
