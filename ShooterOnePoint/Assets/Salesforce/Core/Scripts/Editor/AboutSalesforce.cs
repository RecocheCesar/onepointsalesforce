﻿using UnityEngine;
using UnityEditor;

namespace Salesforce.Editor
{
	public class AboutSalesforce : EditorWindow
	{
		[MenuItem ("SalesforceVR/About", false, 1)]
		public static void  ShowWindow () 
		{
			EditorWindow.GetWindow(typeof(AboutSalesforce));
		}

		[MenuItem("SalesforceVR/Documentation", false, 2)]
        public static void OpenDocumentation()
        {
            string path = Application.dataPath + "/Salesforce/Core/Documentation/HighLevelComponents.pdf";
			if (System.IO.File.Exists(path))
            {
                Application.OpenURL("file://" + path);
            }
            else
            {
                Debug.LogWarning("There is no documentation file at Assets/Salesforce/Core/Documentation/HighLevelComponents.pdf");
            }
        }

		Texture2D logo;
		
		void OnGUI () 
		{
			titleContent.text = "SalesforceVR";
			if (logo == null)
			{
				logo = Resources.Load("SalesforceLogo", typeof(Texture2D)) as Texture2D;
			}
			GUILayout.BeginVertical();
			GUILayout.Label("SalesforceVR", EditorStyles.boldLabel);
			GUILayout.Label("Version 1.0");
			GUILayout.Label(logo);
			GUILayout.EndVertical();
			
		}
	}
}
