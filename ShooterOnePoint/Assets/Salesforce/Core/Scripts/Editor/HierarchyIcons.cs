﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using System.IO;

namespace Salesforce.Editor
{
    [InitializeOnLoad]
    class HierarchyIcons
    {
        static Texture2D texturePanel;
        static List<int> markedObjects;
        static List<string> components;
        static string scriptsPath = "Assets/Salesforce/Core/Scripts";
        static string componentsPath = "Assets/Salesforce/Components/Scripts";

        static HierarchyIcons()
        {
            texturePanel = AssetDatabase.LoadAssetAtPath("Assets/Salesforce/Core/Scripts/Editor/Resources/SalesforceLogo.png", typeof(Texture2D)) as Texture2D;
            markedObjects = new List<int>();
            components = new List<string>();
            GetComponentsFromPath(scriptsPath);
            GetComponentsFromPath(componentsPath);
            EditorApplication.hierarchyWindowItemOnGUI += HierarchyItemCB;
            EditorApplication.hierarchyWindowChanged += OnChange;
            OnChange();
        }

        static void GetComponentsFromPath(string @path)
        {
            if (!Directory.Exists(@path))
            {
                return;
            }
            var dir = new DirectoryInfo(@path);
            foreach (var file in dir.GetFiles("*.cs"))
            {
                components.Add(file.Name.Split('.')[0]);
            }
        }

        static void OnChange()
        {
            markedObjects.Clear();
            GameObject[] go = Object.FindObjectsOfType(typeof(GameObject)) as GameObject[];

            foreach (GameObject g in go)
            {
                if (g == null) continue;

                int instanceId = g.GetInstanceID();

                foreach (var t in components)
                {
                    if (g.GetComponent(t) != null)
                        markedObjects.Add(instanceId);
                }            
            }
        }

        static void HierarchyItemCB(int instanceID, Rect selectionRect)
        {
            Rect r = new Rect(selectionRect);
            r.x += r.width - 18;
            r.width = 20;
            
            if (markedObjects.Contains(instanceID))
            {
                GUI.Label(r, texturePanel);
            }
        }
    }
}
