﻿namespace Salesforce 
{
	public class Field 
	{
		public string Name { get; set; }
		public string Label { get; set; }
		public string Type { get; set; }
		public string Value { get; set; } // JSON.JSONObject
		public string Json { get; set; }
		public RecordList SubRecords { get; set; }

		public Field()
		{
		}
		
		public Field(string name, string value)
		{
			Name = name;
			Value = value;
		}
	}
}
