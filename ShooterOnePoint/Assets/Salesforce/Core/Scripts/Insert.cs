namespace Salesforce 
{
	[System.Serializable]
	public class InsertEvent : UnityEngine.Events.UnityEvent<Response, Record> { }
	
	partial class Client
	{
		public InsertEvent OnInsertSucceeded;
		public FailureEvent OnInsertFailed;

		public void InsertRecord(Record record, System.Action<Response> onFailure, System.Action<Response, Record> onSuccess)
		{
			if (record.Type != null) {
				var json ="";
				foreach (Field f in record) {
					if (json != "")
						json += ",";
					json += "\"" + f.Name + "\":\"" + f.Value +"\"";
				}
				json = "{" + json + "}";
				InsertRecord (record.Type, json, onFailure, onSuccess);
			}
		}

		public void InsertRecord(Record record)
		{
			InsertRecord(record, (response) =>
			{
				OnInsertFailed.Invoke(response);
			},
			(response, data) =>
			{
				OnInsertSucceeded.Invoke(response, data);
			});
		}

		private void InsertRecord(string obj, string record, System.Action<Response> onFailure, System.Action<Response, Record> onSuccess)
		{
			var url = MakeUrl("/sobjects/" + obj + "/");
			var body = System.Text.Encoding.UTF8.GetBytes(record);
			var headers = MakeHeaders("PUT");
			
			StartCoroutine(JsonRequestCoroutine(url, body, headers, (response) =>
			{
				onFailure(response);
			},
			(www, json) =>
			{
				var data = new Record();
				data.Type=obj;
				var field = new Field();
				field.Name = "id";
				field.Value = json.GetString("id");
				data.Add(field);
				onSuccess(new Response(www, json), data);
			}));
		}
	}
}
