﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Salesforce 
{
	public partial class Client
	{
		const string LearningType = "PlayMaker__c";
		const string LearningName = "Name";
		const string LearningCheck = "Check__c";
		const string LearningCompleted = "Completed";
		
		Coroutine learner;
		Queue<string> events = new Queue<string>();

		bool learnerChecked;
		bool learnerEnabled;

		RecordList trackedEvents;

		IEnumerator CheckLearnerCoroutine()
		{
			var completed = false;
			learnerEnabled = false;
			Query("SELECT " + LearningName + ", " + LearningCheck + " FROM " + LearningType,
				(r) => 
				{
					learnerEnabled = false;
					completed = true;
				},
				(r, d) =>
				{
					trackedEvents = d;

					completed = true;
					learnerEnabled = true;
				});
			
			yield return new WaitUntil(() => completed);
		}

		IEnumerator LogEventCoroutine(string eventName)
		{
			foreach (var trackedEvent in trackedEvents)
			{
				if (trackedEvent.GetFieldValue(LearningName) == eventName &&
					trackedEvent.GetFieldValue(LearningCheck) == LearningCompleted)
				{
					yield break;
				}				
			}
			
			var record = new Record(LearningType);
			record.Add(new Field(LearningName, eventName));
			record.Add(new Field(LearningCheck, LearningCompleted));

			var completed = false;
			
			InsertRecord(record,
				(r) => 
				{
					completed = true;
				},
				(r, d) =>
				{
					completed = true;
				});
			
			yield return new WaitUntil(() => completed);
		}

		IEnumerator ProcessQueueCoroutine()
		{
			while (true)
			{
				yield return new WaitForEndOfFrame();

				if (!LoggedIn)
				{
					learnerChecked = false;
					learnerEnabled = false;
					events.Clear();
					continue;
				}

				if (!learnerChecked)
				{
					yield return StartCoroutine(CheckLearnerCoroutine());
					learnerChecked = true;
					continue;
				}

				if (!learnerEnabled)
				{
					events.Clear();
					continue;
				}

				if (events.Count > 0)
				{
					yield return StartCoroutine(LogEventCoroutine(events.Dequeue()));
				}
			}
		}

		void LogDeveloperEvent(string eventName)
		{
			#if UNITY_EDITOR
            var scene = UnityEditor.SceneManagement.EditorSceneManager.GetActiveScene().path;
            if (!scene.StartsWith("Assets/Salesforce/") && 
				UnityEditor.EditorPrefs.GetInt("SalesforceLearningMode", 0) != 0)
            {
                LogEvent(eventName);
            }
            #endif
		}

        void LogDeviceEvent(string eventName)
		{
			#if !UNITY_EDITOR
            LogEvent(eventName);
            #endif
		}

        public void LogEvent(string eventName)
		{
			if (learner == null)
			{
				learner = StartCoroutine(ProcessQueueCoroutine());
			}
			events.Enqueue(eventName);
		}
	}
}
