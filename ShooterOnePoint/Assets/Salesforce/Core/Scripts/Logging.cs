﻿using UnityEngine;

namespace Salesforce 
{
	public partial class Client
	{
		void Log(string message)
		{
			if (!verbose)
			{
				return;
			}
			Debug.Log(message);
		}
		
		void LogWarning(string message)
		{
			if (!verbose)
			{
				return;
			}
			Debug.LogWarning(message);
		}
	}
}
