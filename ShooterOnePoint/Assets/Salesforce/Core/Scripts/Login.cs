﻿using UnityEngine;
using System.Collections;

namespace Salesforce 
{
	[System.Serializable]
	public class LoginEvent : UnityEngine.Events.UnityEvent<Response> { }
	
	public partial class Client
	{
		public LoginEvent OnLoginSucceeded;
		public FailureEvent OnLoginFailed;
		
		public bool LoggedIn { get { return ! string.IsNullOrEmpty(token); } }
		
		public void Login(string username, string password)
		{
			if (string.IsNullOrEmpty(username))
			{
				Debug.LogWarning("[Salesforce] Username is empty");
			}
			if (string.IsNullOrEmpty(password))
			{
				Debug.LogWarning("[Salesforce] Password is empty");
			}

			StartCoroutine(LoginCoroutine(username, password));
		}
		
		IEnumerator LoginCoroutine(string username, string password)
		{
			token = "";
			baseUrl = "";

            /*WWWForm testForm = new WWWForm();
            testForm.AddField("response_type", "code");
            testForm.AddField("client_id", clientId);
            testForm.AddField("redirect_uri", "https://login.salesforce.com/services/oauth2/authorize");

            WWW wwwTest = new WWW("https://login.salesforce.com/services/oauth2/authorize", testForm);
            yield return wwwTest;

            Debug.Log(wwwTest.text);

            if (!string.IsNullOrEmpty(wwwTest.error))
            {
                LogWarning("Test failed: " + wwwTest.error);
                OnLoginFailed.Invoke(new Response(wwwTest));
                yield break;
            }*/


            WWWForm form = new WWWForm ();
			form.AddField("client_id", clientId);
			form.AddField("client_secret", clientSecret);
			form.AddField("grant_type", "password");
			form.AddField("username", username);
			form.AddField("password", password);

            WWW www = new WWW("https://login.salesforce.com/services/oauth2/token", form);

            yield return www;

            Debug.Log(www.text);

			if (!string.IsNullOrEmpty(www.error)) 
			{
				LogWarning("Login failed: " + www.error);
				OnLoginFailed.Invoke(new Response(www));
				yield break;
			}
			
			var response  = JSONObject.Parse(www.text);
			token = response.GetString("access_token");
			baseUrl = response.GetString("instance_url");
			var user = response.GetString ("id").Split ('/');
			userId = user[user.Length-2];
			chatterId = user[user.Length-1];

            PlayerPrefs.SetString("acces_token", token);
            PlayerPrefs.SetString("user_id", userId);

            Log ("Login succeeded: " + userId +"/" + chatterId + " : " + token);
			LogDeveloperEvent("Login");
			LogDeviceEvent("Build");
			OnLoginSucceeded.Invoke(new Response(www));
		}
	}
}
