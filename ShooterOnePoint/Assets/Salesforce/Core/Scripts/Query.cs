using UnityEngine;
using System.Collections.Generic;

namespace Salesforce 
{	
	[System.Serializable]
	public class QueryEvent : UnityEngine.Events.UnityEvent<Response, RecordList> { }
	
	partial class Client
	{
		public QueryEvent OnQuerySucceeded;
		public FailureEvent OnQueryFailed;

		private void Query(string query, System.Action<Response> onFailure, System.Action<Response, RecordList> onSuccess, string nextUrl = null)
		{
			var url = nextUrl ?? MakeUrl("/query/?q=" + WWW.EscapeURL(query));
			var headers = MakeHeaders("PUT");
			Log (url);
			StartCoroutine(JsonRequestCoroutine(url, null, headers, (response) => onFailure(response), (www, json) =>
			{
				Log(www.text);
				var data = new RecordList();
		
				// Subquery Start
				string subQuery = "";
				string subObject = "";
				var pattern = new System.Text.RegularExpressions.Regex(@"\(.*?\)");
				if(pattern.Match(query).Groups.Count==1) {
					subQuery=pattern.Match(query).Groups[0].Value;
				}
				pattern = new System.Text.RegularExpressions.Regex(@"(?<=\b(?i)FROM\s+)\p{L}+");
				if(pattern.Match(subQuery).Groups.Count==1) {
					subObject=pattern.Match(subQuery).Groups[0].Value;
				}
				if(subQuery!=""&&subObject!="") {
					query=query.Replace(subQuery,"");
					subQuery=subQuery.Replace("(","").Replace(")","");
				}
				Log(subObject+":"+subQuery+":"+query);
				// Subquery end

				pattern = new System.Text.RegularExpressions.Regex(@"(?is)SELECT(.*?)(?<!\w*"")(?i)FROM(?!\w*?"")(.*?)(?=WHERE|ORDER|$)"); // <-- Case sensitive....
				var fields = pattern.Match(query).Groups[1].Value.Trim().Split (new char[]{ ',' }, System.StringSplitOptions.RemoveEmptyEntries);
				var subFields = pattern.Match(subQuery).Groups[1].Value.Trim().Split (new char[]{ ',' }, System.StringSplitOptions.RemoveEmptyEntries);

				foreach (var r in json.GetArray("records")) 
				{
					var record = new Record();
					foreach (var fl in fields) 
					{
						string f=fl.Trim();

						var field = new Field();
						field.Name = f;
						field.Value = r.Obj.GetString(f);
						record.Add(field);
					}
					if(subQuery!=""&&subObject!="") {
							// Add sub-records to field and add to record.
							var field = new Field();
							field.Name = subObject;
							field.Value = null;
							field.SubRecords = new RecordList();
							foreach(var s in r.Obj.GetObject(subObject).GetArray("records")) { // <-- Case sensitive?
								var subrec = new Record();
								foreach(var fl in subFields) {
									string f=fl.Trim();
									var subfield = new Field();
									subfield.Name = f;
									subfield.Value = s.Obj.GetString(f);
									subrec.Add(subfield);
								}
								field.SubRecords.Add(subrec);
							}
							record.Add(field);
					}
					record.Type=r.Obj.GetObject("attributes").GetString("type");
					data.Add(record);
				}

				if (json.ContainsKey("totalSize")) {
					data.TotalCount = json.GetInt("totalSize");
				}
				if (json.ContainsKey("nextRecordsUrl")) {
					data.NextURL = json.GetString("nextRecordsUrl");
				}
				onSuccess(new Response(www, json), data);
			}));
		}
		
		public void Query(string query, string nextUrl = null) 
		{
			Query(

				query,

				(response) =>
				{
					OnQueryFailed.Invoke(response);
				},

				(response, data) =>
				{
					if (data.Count > 0)
					{
						LogDeveloperEvent("Query");
					}
					OnQuerySucceeded.Invoke(response, data);
				},
				
				nextUrl);	
		}
	}
}
