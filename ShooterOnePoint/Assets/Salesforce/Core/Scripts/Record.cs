using System.Collections.Generic;

namespace Salesforce 
{
	public class Record : List<Field> 
	{
		public string GetFieldValue(string n, string defaultValue = "")
		{
			foreach (var f in this)
			{
				if (f.Name == n)
				{
					return f.Value;
				}
			}
			return defaultValue;
		}
		
		public void SetFieldValue(string n, string v)
		{
			foreach (var f in this)
			{
				if (f.Name == n)
				{
					f.Value = v;
				}
			}
		}
		
		public RecordList GetSubRecords(string n)
		{
			foreach (var f in this)
			{
				if (f.Name == n)
				{
					return f.SubRecords;
				}
			}
			return null;
		}
		public string Type { get; set; }
		
		public Record(string type = "")
		{
			Type = type;
		}
	}
}
