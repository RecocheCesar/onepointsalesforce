using System.Collections.Generic;

namespace Salesforce 
{
	[System.Serializable]
	public class RecordList : List<Record> 
	{
		public string NextURL { get; set; }
		public int TotalCount { get; set; }
		
		public Record GetRecord(int record)
		{
			if (record < 0 || record >= Count)
			{
				return null;
			}
			return this[record];
		}

		public string GetFieldValue(int record, string fieldName, string defaultValue = "")
		{
			if (record < 0 || record >= Count)
			{
				return defaultValue;
			}
			return this[record].GetFieldValue(fieldName, defaultValue);
		}

		public void SetFieldValue(int record, string fieldName, string value)
		{
			if (record < 0 || record >= Count)
			{
				return;
			}
			this[record].SetFieldValue(fieldName, value);
		}

		public RecordList GetSubRecords(int record, string fieldName)
		{
			if (record < 0 || record >= Count)
			{
				return null;
			}
			return this[record].GetSubRecords(fieldName);
		}
	}
}
