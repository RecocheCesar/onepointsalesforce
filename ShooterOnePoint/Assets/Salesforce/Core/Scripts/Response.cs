﻿using UnityEngine;

namespace Salesforce 
{
	public class Response
	{
		public bool success { get; private set; }
		public string httpStatusCode { get; private set; }
		public string errorCode { get; private set; }
		public string message { get; private set; }
		
		public Response(bool s, string m)
		{
			success = s;
			message = m;
			httpStatusCode = "";
			errorCode = "";
		}
		
		public Response(WWW www)
		{
			if (string.IsNullOrEmpty(www.error))
			{
				success = true;
				message = "";
				httpStatusCode = "";
				errorCode = "";
			}
			else
			{
				success = false;
				message = www.error;
				httpStatusCode = www.error;
				errorCode = "";
			}
		}
		
		public Response(WWW www, JSONObject json) : this(www)
		{
			success = json.GetBoolean("success", success);
			errorCode = json.GetString("errorCode", errorCode);
			message = json.GetString("message", message);			
		}
	}
	
	[System.Serializable]
	public class FailureEvent : UnityEngine.Events.UnityEvent<Response> { }
	
	public partial class Client
	{
		Response NotLoggedIn = new Response(false, "Not Logged In"); 
		Response NotJson = new Response(false, "Responce is not a valid JSON");		
	} 
}
