namespace Salesforce 
{	
	[System.Serializable]
	public class UpdateEvent : UnityEngine.Events.UnityEvent<Response, Record> { }

	public partial class Client
	{
		public UpdateEvent OnUpdateSucceeded;
		public FailureEvent OnUpdateFailed;

		public void UpdateRecord(Record record) 
		{
			if (record.Type != null && !string.IsNullOrEmpty(record.GetFieldValue("id")))
			{
				var json ="";
				foreach (Field f in record)
				{
					if (f.Name == "id")
					{
						continue;
					}

					if (json != "")
						json += ",";
					json += "\"" + f.Name + "\":\"" + f.Value +"\"";
				}
				json = "{" + json + "}";
				UpdateRecord (record.Type, record.GetFieldValue("id"), json);
			}
		}

		public void UpdateRecord(string obj, string id, string data)
		{
			var url = MakeUrl("/sobjects/" + obj + "/" + id+"?_HttpMethod=PATCH");
			var body = System.Text.Encoding.UTF8.GetBytes(data);
			var headers = MakeHeaders("POST");

			StartCoroutine(RequestCoroutine(url, body, headers, (response) =>
			{
				OnUpdateFailed.Invoke(response);
			},
			(www) =>
			{
				LogDeveloperEvent("Update");
				
				var record = new Record();
				record.Type=obj;
				var field = new Field();
				field.Name = "id";
				field.Value = id;
				record.Add(field);
				OnUpdateSucceeded.Invoke(new Response(www), record);
			}));
		}
	}
}
