﻿namespace Salesforce 
{	
	[System.Serializable]
	public class UpsertEvent : UnityEngine.Events.UnityEvent<Response, Record> { }

	public partial class Client
	{
		public UpsertEvent OnUpsertSucceeded;
		public FailureEvent OnUpsertFailed;

		public void UpsertRecord(Record record) {
			Log ("UpsertRecord");
			if (record.Type != null) {
				var json ="";
				foreach (var f in record) {
					if (f.Name.ToLower() != "id") {
						if (json != "")
							json += ",";
						json += "\"" + f.Name + "\":\"" + f.Value + "\"";
					}
				}
				json = "{" + json + "}";
				UpsertRecord (record.Type, record.GetFieldValue("id"), json);
			}
		}

		public void UpsertRecord(string obj, string id, string data)
		{ 
			if (id == "")
				id = null;
			var url = (id!=null) ? MakeUrl("/sobjects/" + obj + "/" + id+"?_HttpMethod=PATCH") : MakeUrl("/sobjects/" + obj + "/");
			var body = System.Text.Encoding.UTF8.GetBytes(data);
			var headers = (id!=null) ? MakeHeaders("POST") : MakeHeaders("PUT");

			StartCoroutine(RequestCoroutine(url, body, headers, (response) =>
			{
				OnUpsertFailed.Invoke(response);
			},
			(www) =>
			{
				var record = new Record();
				record.Type=obj;
				var field = new Field();
				field.Name = "id";
				field.Value = id;
				record.Add(field);
				OnUpsertSucceeded.Invoke(new Response(www), record);
			}));
		}
	}
}
