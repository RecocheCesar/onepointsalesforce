﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ballon : MonoBehaviour {

    [SerializeField]
    private GameObject[]    BallonsMeshs;
    [SerializeField]
    private Vector3         DefaultPosition;
    [SerializeField]
    private TextMesh[]     NameTexts;
    [SerializeField]
    private TextMesh[]     DescTexts;
    [SerializeField]
    private int             m_iCharacterMaxLength = 30;

    private Transform   m_MyTransform;
    private GameObject  m_MyGameObject;
    private Animator    m_MyAnimator;
    private int         m_iCurrentBallonIndex;

	public void InitScript () {
        m_MyTransform = transform;
        m_MyGameObject = gameObject;
        m_MyAnimator = GetComponent<Animator>();
        m_iCurrentBallonIndex = -1;

        Init(true);
    }

    public void Init(bool bActivateMesh)
    {
        m_MyGameObject.SetActive(false);
        m_MyAnimator.SetBool("IsActive", false);
        m_MyTransform.position = DefaultPosition;

        if (bActivateMesh)
            ActivateRandomBallon();
    }

    public void Spawn()
    {
        m_MyGameObject.SetActive(true);
        m_MyAnimator.SetBool("IsActive", true);
        m_MyAnimator.SetTrigger("Reset");
        Fabric.EventManager.Instance.PostEvent("SFX_BalloonArrive");
    }

    public void UpdateOpportunity(OurOpportunity CurrentOpportunity)
    {
        string sClientName = CurrentOpportunity.GetName();
        string sOpportunityDesc = CurrentOpportunity.GetDescription();

        if (sClientName.Length > m_iCharacterMaxLength)
            sClientName = sClientName.Remove(m_iCharacterMaxLength - 1);

        ApplyDescriptionTextRules(ref sOpportunityDesc);
        
        foreach (TextMesh Text in NameTexts)
            Text.text = sClientName;
        foreach (TextMesh Text in DescTexts)
            Text.text = sOpportunityDesc;
    }

    public void DeactivateCurrentMesh()
    {
        if (BallonsMeshs.Length > 0)
        {
            if (m_iCurrentBallonIndex != -1 && m_iCurrentBallonIndex < BallonsMeshs.Length)
                BallonsMeshs[m_iCurrentBallonIndex].SetActive(false);
        }
    }

    private void ApplyDescriptionTextRules(ref string sOpportunityDesc)
    {
        if (sOpportunityDesc.Length > m_iCharacterMaxLength)
        {
            sOpportunityDesc = sOpportunityDesc.Insert(m_iCharacterMaxLength - 1, "\n");

            char nextChar = sOpportunityDesc[m_iCharacterMaxLength];
            if (char.IsWhiteSpace(nextChar))
                sOpportunityDesc = sOpportunityDesc.Remove(m_iCharacterMaxLength, 1);

            if (sOpportunityDesc.Length > (m_iCharacterMaxLength * 2))
                sOpportunityDesc = sOpportunityDesc.Insert((m_iCharacterMaxLength * 2) - 1, "\n");

            if ((m_iCharacterMaxLength * 2) < sOpportunityDesc.Length)
            {
                nextChar = sOpportunityDesc[m_iCharacterMaxLength * 2];
                if (char.IsWhiteSpace(nextChar))
                    sOpportunityDesc = sOpportunityDesc.Remove(m_iCharacterMaxLength * 2, 1);

                if (sOpportunityDesc.Length > (m_iCharacterMaxLength * 3))
                    sOpportunityDesc = sOpportunityDesc.Remove((m_iCharacterMaxLength * 3) - 1);
            }
        }
    }

    private void ActivateRandomBallon()
    {
        if (BallonsMeshs.Length > 0)
        {
            DeactivateCurrentMesh();

            int iRandomIndex = m_iCurrentBallonIndex;

            while (iRandomIndex == m_iCurrentBallonIndex)
                iRandomIndex = Random.Range(0, BallonsMeshs.Length - 1);

            BallonsMeshs[iRandomIndex].SetActive(true);
            m_iCurrentBallonIndex = iRandomIndex;
        }
    }

    public void LaunchIdleSound()
    {
        Fabric.EventManager.Instance.PostEvent("SFX_BalloonStand", Fabric.EventAction.PlaySound);
    }

    public void StopIdleSound()
    {
        Fabric.EventManager.Instance.PostEvent("SFX_BalloonStand", Fabric.EventAction.StopSound);
    }
}