﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTransition : MonoBehaviour {

    [SerializeField]
    private StandBallons            BallonManager;
    [SerializeField]
    private GameObject              State3Canvas;
    [SerializeField]
    private DynamicLayout           DynamicLayoutScript;
    [SerializeField]
    private StandVerif              StandVerifScript;
    [SerializeField]
    private OpportunitySelection    OpportunitySelectionScript;

    private Animator                CameraAnimator;
    private CameraSpeed             CameraSpeedScript;

	void Start () {
        CameraAnimator = GetComponent<Animator>();
        CameraSpeedScript = GetComponent<CameraSpeed>();

        CameraSpeedScript.SetSpeed(0.5f);
    }
	
    public void TransitionState1ToState2()
    {
        if (CameraAnimator)
        {
            CameraAnimator.SetInteger("TransitionIndex", 2);
            CameraSpeedScript.SetSpeed(0.5f);
        }
    }

    public void TransitionState2ToState3()
    {
        if (CameraAnimator)
        {
            CameraAnimator.SetInteger("TransitionIndex", 3);
            CameraSpeedScript.SetSpeed(0.5f);
        }
    }

    public void TransitionState3ToState2()
    {
        if (CameraAnimator)
        {
            CameraAnimator.SetInteger("TransitionIndex", 2);
            CameraSpeedScript.SetSpeed(0.5f);
        }
    }

    public void LaunchState1()
    {
        CameraSpeedScript.SetSpeed(0.0f);
        if (OpportunitySelectionScript)
            OpportunitySelectionScript.Launch();
    }

    public void LaunchState2()
    {
        CameraSpeedScript.SetSpeed(0.0f);
        if (BallonManager)
            BallonManager.Launch();
    }

    public void LaunchState3()
    {
        CameraSpeedScript.SetSpeed(0.0f);
        if (BallonManager && DynamicLayoutScript)
        {
            DynamicLayoutScript.Init();

            List<OurOpportunity> OpportunitiesToClose = BallonManager.GetOpportunitiesToClose();

            for (int i = 0; i < OpportunitiesToClose.Count; i++)
            {
                DynamicLayoutScript.AddElement(OpportunitiesToClose[i]);
            }

            StandVerifScript.Init();
            if (State3Canvas)
                State3Canvas.SetActive(true);
        }
    }
}
