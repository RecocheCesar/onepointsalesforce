﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Text;
using System.Globalization;
using System;
using System.Linq;

public class FakeDataBase : MonoBehaviour
{
    private List<OurOpportunity> AllOpportunities = new List<OurOpportunity>();
    private List<OurOpportunity> ChoosedOppotunities = new List<OurOpportunity>();
    private List<OurOpportunity> FoundOppotunities = new List<OurOpportunity>();
    private List<OurOpportunity> FoundOppotunities2 = new List<OurOpportunity>();
    private List<string> Reasons = new List<string>();

    public List<OurOpportunity> GetAllOpportunities()      { return AllOpportunities; }
    public List<OurOpportunity> GetChoosedOpportunities()  { return ChoosedOppotunities; }
    public List<OurOpportunity> GetFoundOpportunities()    { return FoundOppotunities; }
    public List<OurOpportunity> GetFoundOpportunities2()   { return FoundOppotunities2; }
    public List<string> GetReasons()                    { return Reasons; }

    // [SerializeField]
    private InitiOpportunities  initOp;

    private static FakeDataBase _instance;


    [SerializeField]
    private Transform[] ListPos;

    // Use this for initialization
    void Awake()
    {
        AllOpportunities.Clear();
        ChoosedOppotunities.Clear();
        FoundOppotunities.Clear();
        FoundOppotunities2.Clear();
        
        if (!_instance)
            _instance = this;
        else
            Destroy(this.gameObject);

        /* Reasons.Add("Abandon Client");
         Reasons.Add("Absence de ressource");
         Reasons.Add("No GO");
         Reasons.Add("Non adéquation du profil");
         Reasons.Add("Prix trop élevé");
         Reasons.Add("Autre raison");
         */

        Reasons.Add("Gagnée");
        Reasons.Add("Perdue");


        /*   AllOpportunities.Add(new OurOpportunity("Heijmans NV", "Abdelhak Ilboujjdini", new DateTime(2017,04,13)));
           AllOpportunities.Add(new OurOpportunity("BANQUE DE FRANCE", "AC Windows/Unix", new DateTime(2017, 06, 19)));
           AllOpportunities.Add(new OurOpportunity("BPCE IT", "Accompagnement Big Data", new DateTime(2017, 04, 04)));
           AllOpportunities.Add(new OurOpportunity("CREDIT AGRICOLE CIB", "Accompagnement Change", new DateTime(2017, 05, 05)));
           AllOpportunities.Add(new OurOpportunity("BNPP CIB PARIS", "Accompagnement Démarche", new DateTime(2017, 02, 24)));
           AllOpportunities.Add(new OurOpportunity("DOCONE", "Accompagnement Lionel Lomberget", new DateTime(2017, 01, 02)));
           AllOpportunities.Add(new OurOpportunity("DOCONE", "Accompagnement Lionel Lomberget", new DateTime(2017, 04, 10)));
           AllOpportunities.Add(new OurOpportunity("INSIDE GROUP", "Accompagnement Marie Rives pour LBP", new DateTime(2017, 01, 31)));
           AllOpportunities.Add(new OurOpportunity("AXA - DCOTOP", "Accompagnement projet RH", new DateTime(2017, 04, 06)));
           AllOpportunities.Add(new OurOpportunity("RTL Group", "Accompagnement Sharepoint", new DateTime(2017, 04, 30)));
           AllOpportunities.Add(new OurOpportunity("ETCHART CONSTRUCTION", "Accompagnement texturisation", new DateTime(2017, 06, 26))); //10

           AllOpportunities.Add(new OurOpportunity("BNP Paribas", "Accompagnement transformation", new DateTime(2017, 03, 20)));
           AllOpportunities.Add(new OurOpportunity("BPCE IT", "Accompagnement gestion des demandes", new DateTime(2017, 01, 31)));
           AllOpportunities.Add(new OurOpportunity("Luxair", "Acquisition des licences Nexthink", new DateTime(2017, 04, 28)));
           AllOpportunities.Add(new OurOpportunity("BANQUE DE FRANCE", "Tenue de compte", new DateTime(2017, 06, 16)));
           AllOpportunities.Add(new OurOpportunity("TESSENDERLO", "Admin SQL", new DateTime(2017, 05, 22)));
           AllOpportunities.Add(new OurOpportunity("ONIRIS", "AUDIT", new DateTime(2016, 12, 12)));
           AllOpportunities.Add(new OurOpportunity("La poste Courrier", "Astronaute", new DateTime(2017, 04, 06)));
           AllOpportunities.Add(new OurOpportunity("MBO", "Astronaute Scan", new DateTime(2017, 02, 28)));  //18
           */
    }

    public void AddOpportunity(string id, string oppName, string accName, DateTime closeDate)
    {
        AllOpportunities.Add(new OurOpportunity(id, oppName, accName, closeDate));

        SortByDate(AllOpportunities);
    }

    public void SortByDate(List<OurOpportunity> op) //Tri de la plus ancienne a la plus recente
    {
        op.Sort((a, b) => (a.GetDate().CompareTo(b.GetDate())));
    }

    void Start()
    {
        initOp = GameObject.FindGameObjectWithTag("GameManager").GetComponent<InitiOpportunities>();
    }

    static public FakeDataBase GetInstance()
    {
        if (_instance == null)
            _instance = new FakeDataBase();

        return _instance;
    }

    public void FindOp(string s)
    {
        s = s.ToLower();
        s = RemoveDiacritics(s);

        FoundOppotunities.Clear();
        FoundOppotunities2.Clear();

        if (s == "")
        {
            FoundOppotunities = new List<OurOpportunity>(AllOpportunities);
            FoundOppotunities2 = new List<OurOpportunity>(ChoosedOppotunities);
        }
        else
        {
            for (int i = 0; i < AllOpportunities.Count; ++i)
            {
                if (RemoveDiacritics(AllOpportunities[i].GetName()).ToLower().Contains(s) ||
                    RemoveDiacritics(AllOpportunities[i].GetDescription()).ToLower().Contains(s))
                {
                    FoundOppotunities.Add(AllOpportunities[i]);
                }
            }

            for (int i = 0; i < ChoosedOppotunities.Count; ++i)
            {
                if (RemoveDiacritics(ChoosedOppotunities[i].GetName()).ToLower().Contains(s) ||
                    RemoveDiacritics(ChoosedOppotunities[i].GetDescription()).ToLower().Contains(s))
                {
                    FoundOppotunities2.Add(ChoosedOppotunities[i]);
                }
            }
        }
        initOp.DisplaySelection();
    }

    string RemoveDiacritics(string text)
    {
        var normalizedString = text.Normalize(NormalizationForm.FormD);
        var stringBuilder = new StringBuilder();

        foreach (var c in normalizedString)
        {
            var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
            if (unicodeCategory != UnicodeCategory.NonSpacingMark)
            {
                stringBuilder.Append(c);
            }
        }

        return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
    }

    public void ClearChoosedOpportunity()
    {
        ChoosedOppotunities.Clear();
    }

    public void AddChoosedOpportunity(OurOpportunity NewOpportunity)
    {
        ChoosedOppotunities.Add(NewOpportunity);
    }

    public void SortListByDate(List<OurOpportunity> list)
    {

    }
}
