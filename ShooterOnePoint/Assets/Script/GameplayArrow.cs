﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayArrow : MonoBehaviour
{
    [SerializeField]
    private StandBallons    StandBallonsScript;

    private Animator        MyAnimator;
    private GameObject      ChildMesh;

    void Start()
    {
        if (MyAnimator == null)
            MyAnimator = GetComponent<Animator>();

        if (ChildMesh == null)
            ChildMesh = transform.GetChild(0).gameObject;

        MyAnimator.SetBool("Idle", true);
    }

    public void Throw()
    {
        if (MyAnimator == null)
            MyAnimator = GetComponent<Animator>();

        MyAnimator.SetBool("Throw", true);
        MyAnimator.SetBool("Idle", false);

        Fabric.EventManager.Instance.PostEvent("SFX_Throw");
        //Debug.Log("THROW");
    }

    private void HideArrow()
    {
        if (ChildMesh == null)
            ChildMesh = transform.GetChild(0).gameObject;

        ChildMesh.SetActive(false);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ballon"))
        {
            MyAnimator.SetBool("Throw", false);
            MyAnimator.SetBool("Idle", true);
            HideArrow();
            StandBallonsScript.KillOpportunityInteraction();
        }
    }

    public bool CanThrow()
    {
        bool IsIdle = MyAnimator.GetBool("Idle");
        bool IsThrowing = MyAnimator.GetBool("Throw");

        if (ChildMesh.activeInHierarchy && IsIdle && !IsThrowing)
            return true;
        else
            return false;
    }
}