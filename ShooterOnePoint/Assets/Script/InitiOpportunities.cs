﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InitiOpportunities : MonoBehaviour
{
    [SerializeField]
    private Transform[] ListeOp;

    private FakeDataBase DB;

    [SerializeField]
    private GameObject prefabOp1;
    [SerializeField]
    private GameObject prefabOp2;

    [SerializeField]
    private GameObject Loading;

    // Use this for initialization
    void Start()
    {
        DB = FakeDataBase.GetInstance();
    }

    public void StartInitialization()
    {
        StartCoroutine(FullfillOp());
    }

    public void DisplaySelection()
    {
        for (int i = 0; i < ListeOp[0].childCount; ++i) //RESET
        {
            ListeOp[0].GetChild(i).gameObject.SetActive(false);
            ListeOp[1].GetChild(i).gameObject.SetActive(false);
        }

        for (int j = 0; j < DB.GetFoundOpportunities().Count; ++j) //SETBOX1
        {
            ListeOp[0].GetChild(j).gameObject.SetActive(true);
            ListeOp[0].GetChild(j).GetChild(1).GetComponent<Text>().text = DB.GetFoundOpportunities()[j].GetName();
            ListeOp[0].GetChild(j).GetChild(2).GetComponent<Text>().text = DB.GetFoundOpportunities()[j].GetDescription();
        }

        for (int k = 0; k < DB.GetFoundOpportunities2().Count; ++k) //SETBOX2
        {
            ListeOp[1].GetChild(k).gameObject.SetActive(true);
            ListeOp[1].GetChild(k).GetChild(1).GetComponent<Text>().text = DB.GetFoundOpportunities2()[k].GetName();
            ListeOp[1].GetChild(k).GetChild(2).GetComponent<Text>().text = DB.GetFoundOpportunities2()[k].GetDescription();
        }

    }

    public IEnumerator FullfillOp()
    {
        for (int i = 0; i < DB.GetAllOpportunities().Count; ++i)
        {
            GameObject go = GameObject.Instantiate(prefabOp1, ListeOp[0]);
            GameObject.Instantiate(prefabOp2, ListeOp[1]);

            go.gameObject.SetActive(false);
            go.transform.GetChild(1).GetComponent<Text>().text = DB.GetAllOpportunities()[i].GetName();
            go.transform.GetChild(2).GetComponent<Text>().text = DB.GetAllOpportunities()[i].GetDescription();
            yield return null;
        }

        yield return new WaitForSeconds(1);

        Loading.SetActive(false);
        Fabric.EventManager.Instance.PostEvent("GUI_ShowOpportunities");

        for (int i = 0; i < DB.GetAllOpportunities().Count; ++i)
        {
            ListeOp[0].GetChild(i).gameObject.SetActive(true);
            yield return null;
        }
    }
}



