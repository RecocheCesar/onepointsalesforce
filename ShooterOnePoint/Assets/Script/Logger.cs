﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Logger : MonoBehaviour {

   private SalesforceClient sfc;

    // Use this for initialization
    void Start ()
    {
        sfc = GameObject.FindGameObjectWithTag("Salesforce").GetComponent<SalesforceClient>();
        if (sfc.sf.token != null)
            sfc.LogIn();
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void LogIn()
    {
         sfc.LogIn();
    }
}
