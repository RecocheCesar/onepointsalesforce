﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DynamicLayout : MonoBehaviour {

    [SerializeField]
    private int             iMaxElements = 100;
    [SerializeField]
    private int             iSizeLimit = 9;
    [SerializeField]
    private float           fHeightAddition = 55.0f;
    [SerializeField]
    private GameObject      ElementPrefab;
    [SerializeField]
    private StandVerif      StandVerifScript;

    private RectTransform   MyTransform;

    void Start()
    {
        //Init();
    }

	public void Init ()
    {
        MyTransform = GetComponent<RectTransform>();

        //for (int i = 0; i < 100; i++)
        //{
        //    AddElement(null);
        //}
    }

    public void AddElement(OurOpportunity _Opportunity)
    {
        if (MyTransform && MyTransform.childCount < iMaxElements)
        {
            GameObject Element = Instantiate(ElementPrefab, MyTransform) as GameObject;

            if (Element)
                Element.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            if (MyTransform.childCount >= iSizeLimit)
            {
                MyTransform.offsetMin = new Vector2(MyTransform.offsetMin.x, MyTransform.offsetMin.y - fHeightAddition);
                MyTransform.offsetMax = new Vector2(MyTransform.offsetMax.x, 0.0f);
            }

            if (_Opportunity != null)
            {
                OpportunityElement OpportunityElement = Element.GetComponent<OpportunityElement>();
                OpportunityElement.Init(_Opportunity);
            }
        }
    }
}
