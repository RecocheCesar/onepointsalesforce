﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Loader : MonoBehaviour
{
    [SerializeField]
    private bool bLoadGraphScene = false;

    [SerializeField]
    private GameObject LoadingScreen;

    void Start()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;

        if (bLoadGraphScene)
            SceneManager.LoadScene(3, LoadSceneMode.Additive);

    }

    public void LoadLevel(int i)
    {
        SceneManager.LoadScene(i);
    }

    public void LoadLevelDisplayLoading(int i)
    {
        GameObject.FindGameObjectWithTag("Loading").transform.GetChild(0).gameObject.SetActive(true);
        SceneManager.LoadScene(i);

    }

    public void Quit()
    {
        Fabric.EventManager.Instance.PostEvent("GUI_Clic");
        Application.Quit();
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ValidateOp(GameObject g)
    {
        g.SetActive(false);
        Fabric.EventManager.Instance.PostEvent("Music_Menu", Fabric.EventAction.StopSound);
        Fabric.EventManager.Instance.PostEvent("Music_Game", Fabric.EventAction.PlaySound);
        Fabric.EventManager.Instance.SetGlobalParameter("Verif", 0.0f);
        Fabric.EventManager.Instance.PostEvent("SFX_Start");
    }

    void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "Gameplay_Level_GRAPH")
        {
            LoadingScreen.SetActive(false);
            GameObject.FindGameObjectWithTag("Salesforce").GetComponent<testSF>().FullfillDataBase();

        }

    }

    public void BackToMenu()
    {
        Fabric.EventManager.Instance.PostEvent("GUI_Back");

        //GameObject AudioGO = GameObject.Find("Audio");
        //if (AudioGO != null)
        //    Destroy(AudioGO);

        LoadLevel(1);
    }

    public void VictoryBackToMenu()
    {
        Fabric.EventManager.Instance.PostEvent("SFX_Reward", Fabric.EventAction.StopSound);
        LoadLevel(1);
    }
}
