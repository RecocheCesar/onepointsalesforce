﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour {

    [SerializeField]
    private Loader LoaderScript;

	void Start ()
    {
        if (!Fabric.EventManager.Instance.IsEventActive("Music_Menu", null))
            Fabric.EventManager.Instance.PostEvent("Music_Menu", Fabric.EventAction.PlaySound);

        //if (!Fabric.EventManager.Instance.IsEventActive("SFX_Woosh", null))
            //Fabric.EventManager.Instance.PostEvent("SFX_Woosh", Fabric.EventAction.PlaySound);
        Fabric.EventManager.Instance.SetGlobalParameter("Woosh", 0.0f);
    }
	
    public void LaunchGame()
    {
        Fabric.EventManager.Instance.PostEvent("GUI_Clic");

        if (LoaderScript)
            LoaderScript.LoadLevelDisplayLoading(5);
    }

    public void LaunchVisiteMode()
    {
        Fabric.EventManager.Instance.PostEvent("GUI_Clic");

        if (LoaderScript)
            LoaderScript.LoadLevelDisplayLoading(4);
    }
}