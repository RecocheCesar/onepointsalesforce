﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsManager : MonoBehaviour
{
    private int nbClosed = 0;
    private int _TotalPoints = 0;
    private string PointsKey = "PlayerPoints";

    private static PointsManager _instance;



    void Awake()
    {

        if (!_instance)
            _instance = this;

        else
            Destroy(this.gameObject);


        if (PlayerPrefs.HasKey(PointsKey))
        {
            _TotalPoints = PlayerPrefs.GetInt(PointsKey);
        }
        else
        {
            PlayerPrefs.SetInt(PointsKey, 0);
        }



        GameObject.DontDestroyOnLoad(this.gameObject);
    }


    // Use this for initialization
    void Start ()
    {
    
    }
	
    public int GetTotal()
    {
        return _TotalPoints;
    }
    public int GetNbClosed()
    {
        return nbClosed;
    }
    public void IncNbClosed()
    {
        nbClosed++;
    }
    public void ResetNbClosed()
    {
        nbClosed = 0;
    }

    public void AddPointsToTotal(int points)
    {
        _TotalPoints += points;
        PlayerPrefs.SetInt(PointsKey, _TotalPoints);
    }

    static public PointsManager GetInstance()
    {
        if (_instance == null)
            _instance = new PointsManager();

        return _instance;
    }
}
