﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StandBallons : MonoBehaviour {

    [SerializeField]
    private Ballon                  BallonScript;
    [SerializeField]
    private GameObject              LaunchUI;
    [SerializeField]
    private GameObject              GameplayUI;
    [SerializeField]
    private GameObject              EndGameUI;
    [SerializeField]
    private LayerMask               BallonLayerMask;
    [SerializeField]
    private CameraTransition        CameraTransitionScript;
    [SerializeField]
    private GameObject              Confettis;
    [SerializeField]
    private Transform               RaisonsContent;
    [SerializeField]
    private Sprite[]                RaisonsUI;
    [SerializeField]
    private GameObject              ReasonPrefab;
    [SerializeField]
    private ReasonUIBehaviour       ReasonUIBehaviourScript;

    private HideObjectByTime        m_WarningHideScript;
    private List<OurOpportunity>       m_aOpportunityList;
    private List<OurOpportunity>       m_aOpportunitiesToClose;
    private int                     m_iOpportunityIndex;
    private bool                    m_bGameFinished;
    private ReasonButton            m_SelectedReason;

	void Start () {

    }

    private void Init()
    {
        m_iOpportunityIndex = 0;
        m_bGameFinished = false;
        m_SelectedReason = null;
        m_aOpportunitiesToClose = new List<OurOpportunity>();

        if (BallonScript && m_aOpportunityList != null && m_aOpportunityList.Count > 0)
            BallonScript.UpdateOpportunity(m_aOpportunityList[m_iOpportunityIndex]);
    }

    public List<OurOpportunity> GetOpportunitiesToClose()
    {
        return m_aOpportunitiesToClose;
    }

    public void KillOpportunityInteraction()
    {
        if (m_bGameFinished)
            return;

        if (m_SelectedReason != null)
        {
            Fabric.EventManager.Instance.PostEvent("SFX_Pop");
            BallonScript.StopIdleSound();
            GameObject confetis = Instantiate(Confettis, BallonScript.transform.position, Quaternion.identity);
            StartCoroutine(DestroyLater(confetis, 2.0f));
            AddOpportunityToClose();
            NextOpportunity();
        }
    }

    IEnumerator DestroyLater(GameObject objet, float timer)
    {
        yield return new WaitForSeconds(timer);
        Destroy(objet);
    }

    public void EndGameInteraction()
    {
        Fabric.EventManager.Instance.PostEvent("GUI_Clic");

        if (GameplayUI)
            GameplayUI.SetActive(false);
        if (EndGameUI)
            EndGameUI.SetActive(false);
        if (BallonScript)
            BallonScript.gameObject.SetActive(false);

        if (CameraTransitionScript)
            CameraTransitionScript.TransitionState2ToState3();
    }

    public void LaunchStartCameraTransition()
    {
        if (CameraTransitionScript)
            CameraTransitionScript.TransitionState1ToState2();
    }

    public void Launch()
    {
        m_aOpportunityList = FakeDataBase.GetInstance().GetChoosedOpportunities();

        if (LaunchUI)
            LaunchUI.SetActive(false);
        if (GameplayUI)
            GameplayUI.SetActive(true);

        Init();

        if (BallonScript)
        {
            BallonScript.InitScript();
            BallonScript.Spawn();
        }

        foreach (string sReason in FakeDataBase.GetInstance().GetReasons())
        {
            int iReasonIndex = RaisonsContent.childCount;
            GameObject ReasonButtonGO = Instantiate(ReasonPrefab, RaisonsContent);
            ReasonButton ReasonButton = ReasonButtonGO.GetComponent<ReasonButton>();
            ReasonButton.SetReason(sReason);
        }

      //  ReasonUIBehaviourScript.LaunchGameplay();
    }

    private void NextOpportunity()
    {
        ReasonUIBehaviourScript.DisplayReasons();

        if (m_SelectedReason != null)
            m_SelectedReason.GetComponent<Image>().sprite = RaisonsUI[0];

        if ((m_iOpportunityIndex + 1) >= m_aOpportunityList.Count)
        {
            if (BallonScript)
            {
                BallonScript.Init(false);
                BallonScript.DeactivateCurrentMesh();
            }

            LaunchEndGame();
            return;
        }

        ++m_iOpportunityIndex;

        if (BallonScript)
        {
            BallonScript.Init(true);

            if (m_aOpportunityList != null && m_aOpportunityList.Count > 0)
                BallonScript.UpdateOpportunity(m_aOpportunityList[m_iOpportunityIndex]);

            BallonScript.Spawn();
        //    ReasonUIBehaviourScript.LaunchGameplay();
        }

        m_SelectedReason = null;
    }

    private void LaunchEndGame()
    {
        m_bGameFinished = true;

        if (GameplayUI)
            GameplayUI.SetActive(false);
        if (EndGameUI)
            EndGameUI.SetActive(true);
        Fabric.EventManager.Instance.PostEvent("SFX_Finish");
        Fabric.EventManager.Instance.SetGlobalParameter("Verif", 1.0f);
    }

    private OurOpportunity GetCurrentOpportunity()
    {
        if (m_iOpportunityIndex < m_aOpportunityList.Count)
            return m_aOpportunityList[m_iOpportunityIndex];
        else
            return null;
    }

    void AddOpportunityToClose()
    {
        if (m_iOpportunityIndex < m_aOpportunityList.Count)
        {
            m_aOpportunityList[m_iOpportunityIndex].SetCloseReason(m_SelectedReason.GetReason());
            m_aOpportunitiesToClose.Add(m_aOpportunityList[m_iOpportunityIndex]);
        }
    }

    public void SetSelectedReason(ReasonButton NewSelectedReason)
    {
        if (ReasonUIBehaviourScript.IsOn())
            return;

        if (m_SelectedReason != null)
            m_SelectedReason.GetComponent<Image>().sprite = RaisonsUI[0];
        m_SelectedReason = NewSelectedReason;

        if (m_SelectedReason != null)
        {
            m_SelectedReason.GetComponent<Image>().sprite = RaisonsUI[1];
            ReasonUIBehaviourScript.LaunchGameplay();
            Fabric.EventManager.Instance.PostEvent("SFX_Reason");
        }
    }

    public void CancelCurrentOpportunity()
    {
        Fabric.EventManager.Instance.PostEvent("SFX_BalloonStand", Fabric.EventAction.StopSound);
        Fabric.EventManager.Instance.PostEvent("SFX_Cancel");
        NextOpportunity();
    }
}