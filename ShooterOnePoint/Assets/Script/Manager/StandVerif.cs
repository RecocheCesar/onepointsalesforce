﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandVerif : MonoBehaviour {

    [SerializeField]
    private GameObject              ValidationPopupCanvas;
    [SerializeField]
    private GameObject              CancelOpportunityPopupCanvas;
    [SerializeField]
    private GameObject              VictoryUI;
    [SerializeField]
    private CameraTransition        CameraTransitionScript;
    [SerializeField]
    private GameObject              State3Canvas;
    [SerializeField]
    private Transform               SelectionContent;

    private List<OurOpportunity>       OpportunitiesToClose;
    private StandBallons            BallonManager;
    private GameObject              CurrentElement;

    void Start () {
        BallonManager = GetComponent<StandBallons>();
    }

    public void Init()
    {
        OpportunitiesToClose = new List<OurOpportunity>(BallonManager.GetOpportunitiesToClose());
        for (int i = 0; i < OpportunitiesToClose.Count; i++)
            OpportunitiesToClose[i].bToReplay = false;
    }

    public void DisplayConfirmationPopup(bool bVisible)
    {
        if (bVisible)
            Fabric.EventManager.Instance.PostEvent("GUI_AreYouSure");
        else
            Fabric.EventManager.Instance.PostEvent("GUI_Clic");

        if (ValidationPopupCanvas)
            ValidationPopupCanvas.SetActive(bVisible);
    }

    public void CloseOpportunities()
    {
        FakeDataBase.GetInstance().ClearChoosedOpportunity();
        bool bHaveOpportunityToReplay = false;
        SalesforceClient sfc = GameObject.FindGameObjectWithTag("Salesforce").GetComponent<SalesforceClient>();
        for (int i = 0; i < OpportunitiesToClose.Count; i++)
        {
            if (OpportunitiesToClose[i].bToReplay)
            {
                FakeDataBase.GetInstance().AddChoosedOpportunity(OpportunitiesToClose[i]);
                bHaveOpportunityToReplay = true;
            }
            else
            {
                PointsManager.GetInstance().AddPointsToTotal(10);
                PointsManager.GetInstance().IncNbClosed();

                if(OpportunitiesToClose[i].GetCloseReasonString() == "Perdue")
                   sfc.CloseOpporunity(OpportunitiesToClose[i].GetID(), false);
                else
                   sfc.CloseOpporunity(OpportunitiesToClose[i].GetID(), true);

            }
        }

        RemoveSelectionContent();
        if (bHaveOpportunityToReplay)
        {
            ValidationPopupCanvas.SetActive(false);
            if (State3Canvas)
                State3Canvas.SetActive(false);
            CameraTransitionScript.TransitionState3ToState2();
            Fabric.EventManager.Instance.PostEvent("SFX_Start");
            Fabric.EventManager.Instance.SetGlobalParameter("Verif", 0.0f);
        }
        else
            LaunchVictoryState();
    }

    public void LaunchVictoryState()
    {
        if (State3Canvas)
            State3Canvas.SetActive(false);
        if (ValidationPopupCanvas)
            ValidationPopupCanvas.SetActive(false);
        if (VictoryUI)
        {
            VictoryUI.SetActive(true);
            Fabric.EventManager.Instance.PostEvent("Music_Game", Fabric.EventAction.StopSound);
            Fabric.EventManager.Instance.PostEvent("SFX_Slides");
        }
    }

    public void DisplayCancelOpportunityPopup(bool bVisible)
    {
        Fabric.EventManager.Instance.PostEvent("GUI_Clic");

        if (CancelOpportunityPopupCanvas)
            CancelOpportunityPopupCanvas.SetActive(bVisible);
    }

    public void CancelCloseOpportunity()
    {
        Fabric.EventManager.Instance.PostEvent("GUI_Cancel");
        RemoveOpportunityToClose();
        DisplayCancelOpportunityPopup(false);
    }

    public void ChangeOpportunityReason()
    {
        CurrentElement.GetComponent<OpportunityElement>().SetToReplay(true);
        DisplayCancelOpportunityPopup(false);
        Fabric.EventManager.Instance.PostEvent("SFX_ChangeReason");
    }

    public void SetSelectedElement(GameObject _CurrentElement)
    {
        CurrentElement = _CurrentElement;
    }

    private void RemoveOpportunityToClose()
    {
        //Launch anim
        OpportunityElement Element = CurrentElement.GetComponent<OpportunityElement>();
        Element.transform.GetChild(0).GetComponent<Animator>().SetBool("Destroyed", true);
        Element.HideCancelButton();
        OpportunitiesToClose.Remove(Element.MyOpportunity);
        Destroy(CurrentElement, 1.0f);
    }

    private void RemoveSelectionContent()
    {
        for (int i = 0; i < SelectionContent.childCount; i++)
        {
            Destroy(SelectionContent.GetChild(i).gameObject);
        }
    }
}