﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisiteMode : MonoBehaviour {

    [SerializeField]
    private float                   fTransitionMaxSpeed = 50.0f;
    [SerializeField]
    private Transform[]             CameraPositions;
    [SerializeField]
    private Loader                  LoaderScript;
    [SerializeField]
    private GameObject[]            UIButtons;

    private CameraSpeed             CameraSpeedScript;
    private Transform               MyTransform;
    private Vector3                 StartPosition;
    private Vector3                 TargetPosition;
    private Vector3                 LastPosition = Vector3.zero;
    private Quaternion              StartRotation;
    private Quaternion              TargetRotation;
    private float                   fStartTime;
    private float                   fTransitionLength;
    private bool                    bIsLerping;
    private int                     iCurrentCamPosition;
    private Vector3                 fSpeed = Vector3.zero;

    void Start () {
        CameraSpeedScript = GetComponent<CameraSpeed>();
        CameraSpeedScript.SetSpeed(0.0f);
        MyTransform = transform;
        bIsLerping = false;
        iCurrentCamPosition = 0;
    }
	
	void Update ()
    {
        if (bIsLerping)
        {
            float fDistCovered = (Time.time - fStartTime) * fTransitionMaxSpeed;
            float fFracTransition = fDistCovered / fTransitionLength;
            MyTransform.position = Vector3.SmoothDamp(MyTransform.position, TargetPosition, ref fSpeed, 0.3f, fTransitionMaxSpeed);
            //MyTransform.position = Vector3.Slerp(StartPosition, TargetPosition, fFracTransition);
            MyTransform.rotation = Quaternion.Lerp(StartRotation, TargetRotation, fFracTransition);

            if (fSpeed.magnitude <= 0.1f)
            {
                bIsLerping = false;
                CameraSpeedScript.SetSpeed(0.0f);
                foreach (GameObject Button in UIButtons)
                    Button.SetActive(true);
            }
            else
                CameraSpeedScript.SetSpeed(fSpeed.magnitude);
            
        }
	}

    void LaunchTransition(Transform TargetTransform)
    {
        if (CameraPositions.Length > 0)
        {
            bIsLerping = true;
            fStartTime = Time.time;
            StartPosition = MyTransform.position;
            TargetPosition = TargetTransform.position;
            StartRotation = MyTransform.rotation;
            TargetRotation = TargetTransform.rotation;
            fTransitionLength = Vector3.Distance(StartPosition, TargetPosition);
            CameraSpeedScript.SetSpeed(0.0f);
            foreach (GameObject Button in UIButtons)
                Button.SetActive(false);
        }
    }

    public void NextPosition()
    {
        Fabric.EventManager.Instance.PostEvent("GUI_Clic");

        iCurrentCamPosition++;

        if (iCurrentCamPosition >= CameraPositions.Length)
            iCurrentCamPosition = 0;
  
        LaunchTransition(CameraPositions[iCurrentCamPosition]);
    }

    public void PreviousPosition()
    {
        Fabric.EventManager.Instance.PostEvent("GUI_Clic");

        iCurrentCamPosition--;

        if (iCurrentCamPosition < 0)
            iCurrentCamPosition = CameraPositions.Length - 1;

        LaunchTransition(CameraPositions[iCurrentCamPosition]);
    }

    public void BackToMenu()
    {
        Fabric.EventManager.Instance.PostEvent("GUI_Back");

        if (LoaderScript)
            LoaderScript.LoadLevel(1);
    }
}
