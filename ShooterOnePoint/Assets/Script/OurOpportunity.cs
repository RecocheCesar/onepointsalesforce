﻿using UnityEngine;
using System.Collections;
using System;

public class OurOpportunity
{
    private string _ID;
    private string _Name;
    private string _Description;
    private DateTime _Date;
    private string _sCloseReason = "None";

    public bool bToReplay = false;
    public OurOpportunity(string id, string name, string desc, DateTime date) { _ID = id; _Name = name; _Description = desc; _Date = date; _sCloseReason = "None"; }

    public string GetID()
    {
        return _ID;
    }

    public string GetName()
    {
        return _Name;
    }

    public string GetDescription()
    {
        return _Description;
    }

    public string GetCloseReason()
    {
        return _sCloseReason;
    }

    public DateTime GetDate()
    {
        return _Date;
    }

    public void SetCloseReason(string sNewCloseReason)
    {
        _sCloseReason = sNewCloseReason;
    }

    public string GetCloseReasonString()
    {
        return _sCloseReason;
    }
}