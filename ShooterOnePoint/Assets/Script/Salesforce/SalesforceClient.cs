﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Boomlagoon.JSON;

public class SalesforceClient : MonoBehaviour
{
    [HideInInspector]
    public SFDC.Salesforce sf;

    public InputField username;
    public string _username = "";

    public InputField password;
    public string _password = "";

    public string securityToken;

    public List<string> subObjectQueries;

    private testSF test;

    private void Awake()
    {
        test = GetComponent<testSF>();

        //init object
        sf = gameObject.AddComponent<SFDC.Salesforce>();

    }

    // Use this for initialization
    IEnumerator Log()
    {

        if (_username == "")
        {
            _username = username.text;
            _password = password.text;
        }
        // login
        sf.login(_username, _password + securityToken);

        // wait for Auth Token
        while (sf.token == null)
        {
            yield return null;
        }

      /*  sf.delete("0060Y000009txJDQAY", "Opportunity");

        while (sf.response == null)
        {
            yield return null;
        }

    */
        Debug.Log("REPONSE : "+sf.response);

        string mainQuery = "";
        mainQuery += "SELECT Id, Name, Account.Name, CloseDate, StageName";
        mainQuery += " FROM Opportunity WHERE (IsClosed=false AND IsWon=false AND IsDeleted=false)";

        sf.query(mainQuery);
        while (sf.response == null)
        {
            yield return null;
        }

        Debug.Log("Response from Salesforce: " + sf.response);

        // Extract the JSON encoded value for the Store the procedure ID (Case) in a field 
        // We are using the free add-in from the Unity3D Asset STore called BoomLagoon.

        // Using BoomLagoon, create a JSON Object from the Salesforce response.
        JSONObject json = JSONObject.Parse(sf.response);

        JSONArray records = json.GetArray("records");

        Debug.Log("records = " + records);

        test.DisplayText(records);
    }

    public void LogIn()
    {
        StartCoroutine(Log());
    }

    public void CloseOpporunity(string ID, bool Won)
    {
        if(Won)
        {
            sf.update(ID, "Opportunity", "{ \"StageName\" : \"Closed Won\" }");
        }
        else
        {
            sf.update(ID, "Opportunity", "{ \"StageName\" : \"Closed Lost\" }");
        }
    }

}
