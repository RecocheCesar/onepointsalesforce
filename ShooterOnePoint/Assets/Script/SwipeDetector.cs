﻿using UnityEngine;
using System.Collections;

public class SwipeDetector : MonoBehaviour
{
    [SerializeField]
    private float minSwipeDistY;

    private Vector2 startPos;
    private GameplayArrow GameplayArrowScript;

    private void Awake()
    {
        GameplayArrowScript = GetComponent<GameplayArrow>();
    }

    private void Update()
    {
        if (!GameplayArrowScript.CanThrow())
            return;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameplayArrowScript.Throw();
            return;
        }

        if (Input.touchCount > 0)
        {
            Touch touch = Input.touches[0];

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    startPos = touch.position;
                    break;

                case TouchPhase.Ended:
                    float swipeDistVertical = (new Vector3(0, touch.position.y, 0) - new Vector3(0, startPos.y, 0)).magnitude;
                    if (swipeDistVertical > minSwipeDistY)
                    {
                        float swipeValue = Mathf.Sign(touch.position.y - startPos.y);
                        if (swipeValue > 0)
                            GameplayArrowScript.Throw();
                    }
                    break;
            }

        }
    }

}