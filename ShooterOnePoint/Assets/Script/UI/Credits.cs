﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Credits : MonoBehaviour {

    [SerializeField]
    private GameObject StartCanvas;
    [SerializeField]
    private GameObject CreditsCanvas;

    void Start () {
		
	}
	
	void Update () {
		
	}

    public void Launch()
    {
        Fabric.EventManager.Instance.PostEvent("GUI_Clic");
        if (StartCanvas)
            StartCanvas.SetActive(false);
        if (CreditsCanvas)
            CreditsCanvas.SetActive(true);
    }

    public void BackToMenu()
    {
        Fabric.EventManager.Instance.PostEvent("GUI_Back");

        if (StartCanvas)
            StartCanvas.SetActive(true);
        if (CreditsCanvas)
            CreditsCanvas.SetActive(false);
    }
}
