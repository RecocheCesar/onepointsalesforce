﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FunkyTextAnim : MonoBehaviour
{
    private Text _Text;

    void Start()
    {
        _Text = GetComponent<Text>();

        StartCoroutine(OuuuhBaby());
    }

    IEnumerator OuuuhBaby()
    {
        int _length = _Text.text.Length - 3;
        bool _isLower = false;

        while (true)
        {
            if (!_isLower)
            {
                for (int i = 0; i < _length; ++i)
                {
                    yield return new WaitForSeconds(0.16f);
                    string temp = _Text.text.Replace(_Text.text.ToCharArray()[i], _Text.text.ToLower().ToCharArray()[i]);
                    _Text.text = temp;
                    _isLower = true;
                }
            }
            else
            {
                for (int i = 0; i < _length; ++i)
                {
                    yield return new WaitForSeconds(0.16f);
                    string temp = _Text.text.Replace(_Text.text.ToCharArray()[i], _Text.text.ToUpper().ToCharArray()[i]);
                    _Text.text = temp;
                    _isLower = false;
                }
            }
        }
    }
}
