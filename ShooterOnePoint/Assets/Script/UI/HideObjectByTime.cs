﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideObjectByTime : MonoBehaviour {

    [SerializeField]
    private float m_fTimeToHide = 5.0f;
    private float m_fHideTimer;

	void Start () {
        m_fHideTimer = m_fTimeToHide;
    }

    void Update()
    {
        if (m_fHideTimer > 0.0f)
            m_fHideTimer -= Time.deltaTime;
        else
        {
            m_fHideTimer = m_fTimeToHide;

            gameObject.SetActive(false);
        }
    }

    public void ResetTimer()
    {
        m_fHideTimer = m_fTimeToHide;
    }
}
