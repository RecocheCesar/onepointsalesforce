﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpportunityElement : MonoBehaviour {

    private StandVerif StandVerifScript;
    private Button CancelButton;
    private GameObject ToReplayText;
    public OurOpportunity MyOpportunity;

    public void Init (OurOpportunity NewOpportunity)
    {
        MyOpportunity = NewOpportunity;

        StandVerifScript = FindObjectOfType<StandVerif>();
        Transform myTransform = transform;
        Text DescText = myTransform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>();
        Text TitleText = myTransform.GetChild(0).GetChild(0).GetChild(1).GetComponent<Text>();
        Text ReasonText = myTransform.GetChild(0).GetChild(0).GetChild(2).GetComponent<Text>();
        CancelButton = myTransform.GetChild(0).GetChild(1).GetComponent<Button>();
        ToReplayText = myTransform.GetChild(0).GetChild(2).gameObject;

        TitleText.text = NewOpportunity.GetName();
        DescText.text = NewOpportunity.GetDescription();
        ReasonText.text = NewOpportunity.GetCloseReasonString();
        CancelButton.onClick.AddListener(CancelButtonClicked);
    }
	
	void CancelButtonClicked()
    {
        if (StandVerifScript)
        {
            StandVerifScript.SetSelectedElement(gameObject);
            StandVerifScript.DisplayCancelOpportunityPopup(true);
        }
    }

    public void HideCancelButton()
    {
        CancelButton.gameObject.SetActive(false);
    }

    public void SetToReplay(bool bToReplay)
    {
        MyOpportunity.bToReplay = bToReplay;
        ToReplayText.SetActive(true);
        HideCancelButton();
    }
}