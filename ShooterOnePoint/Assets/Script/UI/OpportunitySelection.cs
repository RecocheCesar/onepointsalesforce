﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpportunitySelection : MonoBehaviour
{
    [SerializeField]
    private GameObject Etape1Canvas;
    [SerializeField]
    private Transform[] ListePos;

    [SerializeField]
    private Text search;

    private FakeDataBase DB;

    [SerializeField]
    private GameObject Valider;

    // Use this for initialization
    void Start()
    {
        DB = FakeDataBase.GetInstance();
    }

    public void GoToSelected(Transform t)
    {
        Fabric.EventManager.Instance.PostEvent("GUI_AddOpportunity");

        OurOpportunity op = new OurOpportunity("","","", new System.DateTime());

        foreach (OurOpportunity o in DB.GetAllOpportunities())
        {
            if (o.GetName() == t.GetChild(1).GetComponent<Text>().text && o.GetDescription() == t.GetChild(2).GetComponent<Text>().text)
            {
                op = o;
                break;
            }
        }

        if (DB.GetChoosedOpportunities().Count == 0)
            Valider.SetActive(true);

        DB.GetChoosedOpportunities().Add(op);
        DB.GetAllOpportunities().Remove(op);

        DB.FindOp(search.text);
    }

    public void ComeBackToSelected(Transform t)
    {
        Fabric.EventManager.Instance.PostEvent("GUI_DiscardOpportunity");

        OurOpportunity op = new OurOpportunity("","", "", new System.DateTime());

        foreach (OurOpportunity o in DB.GetChoosedOpportunities())
        {
            if (o.GetName() == t.GetChild(1).GetComponent<Text>().text && o.GetDescription() == t.GetChild(2).GetComponent<Text>().text)
            {
                op = o;
                break;
            }
        }

        DB.GetAllOpportunities().Add(op);
        DB.GetChoosedOpportunities().Remove(op);

        if (DB.GetChoosedOpportunities().Count == 0)
            Valider.SetActive(false);

        DB.FindOp(search.text);
    }

    public void Launch()
    {
        if (Etape1Canvas)
        {
            Etape1Canvas.SetActive(true);
            gameObject.GetComponent<InitiOpportunities>().StartInitialization();
        }
    }
}
