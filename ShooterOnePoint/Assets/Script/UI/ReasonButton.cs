﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReasonButton : MonoBehaviour {
    
    private string          m_sReason = "None";
    private StandBallons    StandBallonScript;
    private Text            ReasonTextUI;

    void Start()
    {
        StandBallonScript = GameObject.Find("GameManager").GetComponent<StandBallons>();
    }

    public string GetReason()
    {
        return m_sReason;
    }

    public void SetReason(string sNewReason)
    {
        if (ReasonTextUI == null)
            ReasonTextUI = GetComponentInChildren<Text>();

        m_sReason = sNewReason;
        ReasonTextUI.text = m_sReason;
    }

    public void Select()
    {
        if (m_sReason != "None" && StandBallonScript)
            StandBallonScript.SetSelectedReason(this);
    }
}