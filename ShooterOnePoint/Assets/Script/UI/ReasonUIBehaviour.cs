﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReasonUIBehaviour : MonoBehaviour {

    [SerializeField]
    private GameObject      ArrowButtonGO;
    [SerializeField]
    private GameObject      ArrowMeshGO;
    [SerializeField]
    private Transform       SpawnTransform;
    [SerializeField]
    private GameObject      CancelButton;
    
    private Animator        MyAnimator;

    [SerializeField]
    private GameObject Hand;


    void Start () {
        MyAnimator = GetComponent<Animator>();
    }

    public void LaunchGameplay()
    {
        MyAnimator.SetTrigger("Hide");
      //  Hand.SetActive(false);
        StartCoroutine(WaitBeforeHint(3.0f));

        if (CancelButton)
            CancelButton.SetActive(false);

        if (ArrowMeshGO)
            ArrowMeshGO.SetActive(true);
    }

    public void DisplayArrowButton()
    {
        if (ArrowButtonGO)
            ArrowButtonGO.SetActive(true);
    }

    public void DisplayReasons()
    {
        if (!IsOn())
            return;

        if (ArrowButtonGO)
            ArrowButtonGO.SetActive(false);

        if (CancelButton)
            CancelButton.SetActive(true);

        MyAnimator.SetTrigger("Display");
        StopAllCoroutines();
        Hand.SetActive(false);


        if (ArrowMeshGO)
            ArrowMeshGO.SetActive(false);
    }

    public bool IsOn()
    {
        return (ArrowButtonGO.activeInHierarchy);
    }

    IEnumerator WaitBeforeHint(float time)
    {
        yield return new WaitForSeconds(time);
        Hand.SetActive(true);
    }
}
