﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Splashscreen : MonoBehaviour
{
    private Image _Image;

    [SerializeField]
    private float _TimeToWait;

    // Use this for initialization
    void Start()
    {
        Application.targetFrameRate = 30;

        _Image = GetComponent<Image>();
        StartCoroutine(FadeIn());
    }

    IEnumerator WaitForEndSplashScreen()
    {
        yield return new WaitForSeconds(_TimeToWait);
        StartCoroutine(FadeOut());
    }

    IEnumerator FadeIn()
    {
        for (float i = 0; i < 1; i += 0.015f)
        {
            yield return null;
            _Image.color = new Color(1, 1, 1, i);
        }

        _Image.color = new Color(1, 1, 1, 1);

        StartCoroutine(WaitForEndSplashScreen());

    }

    IEnumerator FadeOut()
    {
        for (float i = 1; i > 0; i -= 0.015f)
        {
            yield return null;
            _Image.color = new Color(1, 1, 1, i);
        }

        _Image.color = new Color(1, 1, 1, 0);

        SceneManager.LoadScene(1);
    }

}
