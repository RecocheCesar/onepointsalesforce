﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateAlphaText : MonoBehaviour {

    [SerializeField]
    private float m_fFadeDuration = 1.0f;


    private float m_fFadeTimer;
    private Text m_TextComponent;
    private bool m_bFadeIn = false;

	void Start () {
        m_fFadeTimer = 0.0f;

        m_TextComponent = GetComponent<Text>();
    }

	void Update () {
        if (m_fFadeTimer > 0.0f)
            m_fFadeTimer -= Time.deltaTime;
        else
        {
            m_fFadeTimer = m_fFadeDuration;

            if (m_TextComponent)
            {
                if (m_bFadeIn)
                { m_TextComponent.CrossFadeAlpha(1.0f, m_fFadeDuration, true); m_bFadeIn = false; }
                else
                { m_TextComponent.CrossFadeAlpha(0.0f, m_fFadeDuration, true); m_bFadeIn = true; }
            }
        }
    }
}
