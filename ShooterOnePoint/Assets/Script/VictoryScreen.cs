﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VictoryScreen : MonoBehaviour
{

    [SerializeField]
    private Text NbClosed;
    [SerializeField]
    private Text Points;
    [SerializeField]
    private Text Total;

    private int finished = 0;

    private void OnEnable()
    {
        NbClosed.text = PointsManager.GetInstance().GetNbClosed().ToString();
        Points.text = (PointsManager.GetInstance().GetNbClosed() * 10).ToString();
        Total.text = (PointsManager.GetInstance().GetTotal() - PointsManager.GetInstance().GetNbClosed() * 10).ToString();

        StartCoroutine(Translations(NbClosed,0));
        StartCoroutine(Translations(Points,0.3f));
        StartCoroutine(Translations(Total,0.6f));

    }

    IEnumerator Translations(Text t, float Timer)
    {
        yield return new WaitForSeconds(Timer);

        for (int i = 544; i> 16; i-=34)
        {
            t.rectTransform.parent.localPosition = new Vector3(i, t.rectTransform.parent.localPosition.y, 0);
            yield return null;
        }

        finished++;
        if(finished == 3)
        {
            StartCoroutine(EmptyScore());
        }
    }

    IEnumerator EmptyScore()
    {

        yield return new WaitForSeconds(0.7f);
        Fabric.EventManager.Instance.PostEvent("SFX_Reward", Fabric.EventAction.PlaySound);
        int won = PointsManager.GetInstance().GetNbClosed() * 10;

        for(int i = 1; i<= won; ++i)
        {
            Points.text = (won - i).ToString();
            Total.text = (Int32.Parse(Total.text) + 1).ToString();
            yield return null;
        }
        Fabric.EventManager.Instance.PostEvent("SFX_Reward", Fabric.EventAction.StopSound);

        PointsManager.GetInstance().ResetNbClosed();
    }

}
