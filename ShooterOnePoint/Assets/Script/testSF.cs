﻿using Boomlagoon.JSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class testSF : MonoBehaviour
{

    List<Opportunity> oppList = new List<Opportunity>();

    private static testSF _instance;

    private void Awake()
    {
        if (!_instance)
            _instance = this;
        else
        {
            Destroy(this.gameObject);
        }
    }

    // Use this for initialization
    void Start()
    {
        GameObject.DontDestroyOnLoad(this.gameObject);
    }

    public void DisplayText(JSONArray opportunityRecords)
    {
        oppList.Clear();
        foreach (JSONValue row in opportunityRecords)
        {
            JSONObject rec = JSONObject.Parse(row.ToString());

            Opportunity opp = Opportunity.CreateInstance("Opportunity") as Opportunity;
            opp.init(rec);
            oppList.Add(opp);

            Debug.Log(opp.Id +"/ "+opp.oppName + "/" + opp.account.Name + "/" + opp.closeDate + "/" + opp.stageName);
        }

        GameObject.FindGameObjectWithTag("Loading").transform.GetChild(0).gameObject.SetActive(true);
        SceneManager.LoadScene("Gameplay_Level");
    }

    public void FullfillDataBase()
    {
        FakeDataBase fdb = GameObject.FindGameObjectWithTag("Database").GetComponent<FakeDataBase>();

        for(int i = 0; i< oppList.Count; ++i)
        {
            fdb.AddOpportunity(oppList[i].Id, oppList[i].oppName, oppList[i].account.Name, 
                new System.DateTime
                (
                Int32.Parse(""+oppList[i].closeDate[0]+ oppList[i].closeDate[1]+ oppList[i].closeDate[2]+oppList[i].closeDate[3]),
                Int32.Parse(""+oppList[i].closeDate[5]+ oppList[i].closeDate[6]), 
                Int32.Parse(""+oppList[i].closeDate[8]+ oppList[i].closeDate[9])
                )
                );
        }
    }

    static public testSF GetInstance()
    {
        if (_instance == null)
            _instance = new testSF();

        return _instance;
    }
}
